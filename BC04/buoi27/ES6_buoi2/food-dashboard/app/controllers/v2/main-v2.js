import { layThongTinTuForm, renderDanhSachMonAn, showMonAnLenForm } from "./controller-v2.js";

let danhSachMonAn = [];

document.getElementById('btnThemMon').addEventListener('click', function(){
    let monAn = layThongTinTuForm();
    console.log('MonAnV2: ', monAn);
    danhSachMonAn.push(monAn);

    renderDanhSachMonAn(danhSachMonAn);

});

function xoaMonAn(id) {
    console.log('id: ', id);
    let index = danhSachMonAn.findIndex((item)=>{
        return item.ma == id;
    });
   console.log('index: ', index);
   if (index != -1){
    danhSachMonAn.splice(index, 1);
    renderDanhSachMonAn(danhSachMonAn);
   }
}

// function tạo trong Module không truyền ra ngoài được, nên cần thông qua 1 object lớn nhất là 'window'
window.xoaMonAn = xoaMonAn;

function suaMonAn(id) {
    console.log('id: ', id);
    let index = danhSachMonAn.findIndex((item)=>{
        return item.ma == id;
    });
   console.log('index: ', index);
   if (index != -1){
    showMonAnLenForm(danhSachMonAn[index])
   }    
}
window.suaMonAn = suaMonAn;
