import { MonAnV2 } from "../../models/v2/monAnModel-v2.js";

export function layThongTinTuForm() {
  let ma = document.getElementById("foodID").value;
  let ten = document.getElementById("tenMon").value;
  let loai = document.getElementById("loai").value;
  let gia = document.getElementById("giaMon").value;
  let khuyenMai = document.getElementById("khuyenMai").value;
  let tinhTrang = document.getElementById("tinhTrang").value;
  let hinhMon = document.getElementById("hinhMon").value;
  let moTa = document.getElementById("moTa").value;

  let monAn = new MonAnV2(
    ma,
    ten,
    loai,
    gia,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  );
  return monAn;
}

export function renderDanhSachMonAn(foodList) {
  let contentHTML = "";
  foodList.forEach((item) => {
    let contentTr = `
        <tr>
        <td>${item.ma}</td>
        <td>${item.ten}</td>
        <td>${item.loai == "chay" ? "Chay" : "Mặn"}</td>
        <td>${item.gia}</td>
        <td>${item.phanTramKm * 100}%</td>
        <td>${item.tinhGiaKm()}</td>
        <td>${item.tinhTrang == 0 ? "Hết" : "Còn"}</td>
        <td>
            <button onclick=xoaMonAn("${
              item.ma
            }") class='btn btn-danger'>Xóa</button>
            <button
            data-toggle='modal'
            data-target='#exampleModal'
            onclick=suaMonAn("${item.ma}") 
            class='btn btn-warning'>Sửa</button>
        </td>
            </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
}

export let showMonAnLenForm = (monAn) => {
  let { ma, ten, loai, gia, phanTramKm, tinhTrang, hinhMon, moTa } = monAn;

  document.getElementById("foodID").value = ma;
  document.getElementById("tenMon").value = ten;
  document.getElementById("loai").value = loai;
  document.getElementById("giaMon").value = gia;
  document.getElementById("khuyenMai").value = phanTramKm;
  console.log('khuyenMai:===== ', khuyenMai);
  document.getElementById("tinhTrang").value = tinhTrang;
  document.getElementById("hinhMon").value = hinhMon;
  document.getElementById("moTa").value = moTa;
};
