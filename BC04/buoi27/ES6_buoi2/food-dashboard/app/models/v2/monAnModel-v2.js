export class MonAnV2 {
    constructor(ma, ten, loai, gia, phanTramKm, tinhTrang, hinhMon, moTa){
        this.ma = ma;
        this.ten = ten;
        this.loai = loai;
        this.gia = gia;
        this.phanTramKm = phanTramKm;
        this.tinhTrang = tinhTrang;
        this.hinhMon = hinhMon;
        this.moTa = moTa;
    }
    tinhGiaKm = function () {
        return this.gia * (1 - this.phanTramKm);
    }
}