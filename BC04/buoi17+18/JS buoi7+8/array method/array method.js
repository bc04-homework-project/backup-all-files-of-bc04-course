var numArr = ['2', '4', '6'];
function getNum(item) {
    console.log({item})
}
numArr.forEach(getNum);

// .forEach dùng để hỗ trợ duyệt mảng, thường dùng để show thông tin
//cách giang hồ hay viết forEach
numArr.forEach(function (item, index) {
    console.log({item, index})
})

var number7 = [65, 44, 12, 4];
var sum7 = 0;
console.log('sum7: ', sum7);
console.log('number7: ', number7);
// number7.forEach(myFunction)
// function myFunction(item, index, arr) {
//   arr[index] = item * 10;
// }
// console.log('number 7: ', number7);
number7.forEach(function (item) {
    console.log('item: ', item);

   sum7 += item;
});
console.log('number 7: ', number7);
console.log('sum7: ', sum7);



// .map dùng duyệt mảng và hỗ trợ return ~ tạo ra mảng mới ~ từ dữ liệu thô sang dữ liệu cần thiết
var resultArr = numArr.map(function (number) {
    console.log('numberrr: ', number);
    return number*10;
})
console.log('resultArr: ', resultArr);


// Mutable - Immutable = các thuộc tính của mảng mà có tác động hoặc không tác động tới giá trị trong mảng
// push pop

var menu = ['trà sữa', 'cafe']

menu.push('trà đá')
console.log('menu - ', menu);
console.log({menu});

var lastItem = menu.pop();
console.log('lastItemm - ', lastItem);
console.log('menu - ', menu);
console.log({menu});

// shift unshift
menu.unshift('milk teaa');
console.log('menu - ', menu);


// CRUD - thêm bớt xóa sửa

// indexOf ~ tìm vị trí - có thể ứng dụng cho chức năng tìm kiếm khi user nhập dữ liệu để tìm

var indexCafe = menu.indexOf('cafe');
console.log('indexCafe: ', indexCafe);

if (indexCafe != -1) {
    console.log('tìm thấy');
}else {
    console.log('tìm 00000 thấy');
}

var numbers = [3, 6, 8, 9, 10];
console.log('numbers: ', numbers);
var result3 = numbers.filter(function (item) {
    return item < 8;
})
console.log('result3: ', result3);

var result2 = numbers.slice(1, 3);
console.log('result2: ', result2);
console.log('numbers: ', numbers);

var result1 = numbers.splice(indexCafe, 1);
console.log('result1: ', result1);
console.log('numbers: ', numbers);


