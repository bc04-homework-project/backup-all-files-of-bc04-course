// Cách viết các hàm Call Back

function introduce(username, calbak) {
    calbak(username);
    console.log('Have a nice day');
}

function sayHello(username) {
    console.log('Hello ' + username);
}

function sayGoodbye(user) {
    console.log('Goodbye to ' + user);
}

introduce('Aalicee', sayHello)
introduce('BBooobb', sayGoodbye)



// tess1
console.log('tess1');

function testCallBack1(name1, call1) {
    call1(name1);
    console.log('call 001', name1)
}

function beCalled101(name1) {
    beCalled102(name1);
    console.log('be called 01', name1);
}

function beCalled102(name1) {
    // call102(name1);
    console.log('be called 02', name1);
}

testCallBack1('Tommm', beCalled101);
console.log('case 2---')
testCallBack1('Tommm', beCalled102);




// tess2
console.log('tess2');

function testCallBack2(name2, call2, call202) {
    call202(name2);
    call2(name2);
    console.log('call 002', name2)
}

function beCalled201(name2) {
    // beCalled202(name2);
    console.log('be called 01', name2);
}

function beCalled202(name2) {
    // call102(name2);
    console.log('be called 02', name2);
}

testCallBack2('Jerrryyy', beCalled201, beCalled202);



// tess3

// console.log(
function introduce2(username2, calbak2) {
    calbak2(username2);
    console.log('Have a nice day');
};

function sayHello2(username2) {
    console.log('Hello ' + username2);
};

function sayGoodbye2(user2) {
    console.log('Goodbye to ' + user2);
};

introduce2('Aalicee2', sayHello2) 
console.log( introduce2('BBooobb2', sayGoodbye2) )

// )