// var tdList = document.querySelectorAll(".td-scores");
// console.log("tdList: ", tdList);

// for (var index = 0; index < tdList.length; index++) {
//   var currentIndex = tdList[index];
//   console.log(currentIndex.innerText);
// }

// var trList = document.querySelectorAll("#tblBody tr");
// console.log("trList: ", trList);

// var trResult = trList[0];

// var tdListResult = trResult.querySelectorAll("td");
// console.log("tdListResult: ", tdListResult);
// console.log(tdListResult[3].innerText);

//

function getScoreFromTr(trTag) {
  var tdList = trTag.querySelectorAll("td");
  return tdList[3].innerText * 1;
}

function getNameFromTr(trTag) {
  var tdList = trTag.querySelectorAll("td");
  return tdList[2].innerText;
}

var trList = document.querySelectorAll("#tblBody tr");

// Tìm sinh viên giỏi nhất - giá trị lớn nhất
var trMax = trList[0];

for (var index = 1; index < trList.length; index++) {
  var currentTr = trList[index];
  // Điểm thẻ Tr hiện tại
  var currentScore = getScoreFromTr(currentTr);
  // điểm thẻ Tr lớn nhất
  var currentScoreMax = getScoreFromTr(trMax);

  if (currentScore > currentScoreMax) {
    trMax = currentTr;
  }
}
console.log("trMax: ", trMax);

// var result1 = getScoreFromTr(trMax);
// console.log("result: ", result1);

// var result2 = getNameFromTr(trMax);
// console.log("result: ", result2);

// document.getElementById('svGioiNhat').innerHTML = result2;
document.getElementById('svGioiNhat').innerHTML = `${getNameFromTr(trMax)} - ${getScoreFromTr(trMax)}`;



// Tìm sinh viên yếu nhất - giá trị nhỏ nhất
var trMin = trList[0];

for (var index = 1; index < trList.length; index++) {
  var currentTr = trList[index];
  // Điểm thẻ Tr hiện tại
  var currentScore = getScoreFromTr(currentTr);
  // điểm thẻ Tr nhỏ nhất
  var currentScoreMin = getScoreFromTr(trMin);

  if (currentScore < currentScoreMin) {
    trMin = currentTr;
  }
}
console.log("trMin: ", trMin);

// var result3 = getScoreFromTr(trMin);
// console.log("result: ", result3);

// var result4 = getNameFromTr(trMin);
// console.log("result: ", result4);

// document.getElementById('svYeuNhat').innerHTML = result4;
document.getElementById('svYeuNhat').innerHTML = `${getNameFromTr(trMin)} - ${getScoreFromTr(trMin)}`;


// yêu cầu 2
//21. số sinh viên giỏi

//dùng Array.from để chuyển giá trị thành mảng
var arrSinhVienGioi = Array.from(trList).filter(function (trTag) {

  return getScoreFromTr(trTag) >= 8;
})
  console.log('arrSinhVienGioi: ', arrSinhVienGioi);

