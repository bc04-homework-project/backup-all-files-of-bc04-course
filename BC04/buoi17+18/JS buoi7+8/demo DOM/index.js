// querySelector chỉ trả về duy nhất đối tượng đầu tiên phù hợp
document.querySelector('.title').style.color = 'red';

var tess4 = document.querySelector('.title').innerHTML;
console.log('tess4: ', tess4);
var tess5 = document.querySelectorAll('.title').innerHTML;
console.log('tess5: ', tess5);


// có thể gọi đến object như cách của CSS
document.querySelector('.header p').style.color = 'blue';

// querySelectorAll (thay thế tốt hơn cho cách gọi getElementByName/Class) dù trả về nhiều giá trị hay chỉ 1 giá trị cũng tạo thành một mảng array
var titleList = document.querySelectorAll('.title');
console.log('titleList: ', titleList);

titleList[5].style.color = 'green';
titleList[0,1,2,7].style.color = 'yellow'; //bỏ nhiều index vô nhưng cũng chỉ lấy phần tử cuối cùng

console.log('tess1')

for ( indexx=0; indexx< titleList.length; indexx++){
    titleList[indexx].className = 'titeee alert alert-info';
    titleList[indexx].innerHTML = 'ok con dee';
}

titleList[0].innerHTML = 'ok BD';

var titleLisss = document.querySelectorAll('.title');
console.log('titleLisss: ', titleLisss); // đã thay class trong loop phía trên nên không DOM được nữa 





//test array method -> kết quả tạm thời cho thấy các element lấy từ HTML thể hiện thông tin trong console có Prototype là Nodelist khác với array tạo ra trong JS có Prototype là Array, do đó có những lệnh/tính năng sử dụng không giống nhau.

console.log('tess2')

titleList.forEach(function (itemm, indexx) {
    console.log({itemm, indexx})
});





// titleList.map(function (item) {
//     console.log({item});
//     // return item;
// });

// var arrTitle = titleList.map(function (item, index) {
//     console.log({item, index});
//     // return item;
// });

// titleList.push('tess1');



var titleLis = [2, 3, 4, 5, 7]
console.log('titleLis: ', titleLis);

titleLis.pop();
// var lastItem = titleLis.pop();
// console.log('lastItem: ', lastItem);
console.log('titleLis: ', titleLis);

titleLis.push('tess2');
console.log('titleLis: ', titleLis);
