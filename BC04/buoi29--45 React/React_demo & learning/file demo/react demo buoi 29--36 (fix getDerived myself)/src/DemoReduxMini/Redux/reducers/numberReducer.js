import { GIAM_SO_LUONG, TANG_SO_LUONG } from "../const/numberConst";

let initialState = {
    soLuong: 100,
    isLogin: 'true',
}


export let numberReducer = (state4 = initialState, action) => {
    switch (action.type){
        case TANG_SO_LUONG: {
            state4.soLuong++;
            return {...state4};
        };
        case GIAM_SO_LUONG: {
            state4.soLuong += action.payload;
            return {...state4};
        }
        case 'cong5': {
            state4.soLuong+=5;
            return {...state4};
        };

        default: {
            return state4;
        }
    }
};
