import React, { Component } from "react";
import { connect } from "react-redux";
import { tangSoLuongAction } from "./Redux/actions/numberAction";
import { GIAM_SO_LUONG, TANG_SO_LUONG } from "./Redux/const/numberConst";

class DemoReduxMini extends Component {
  render() {
    return (
      <div className="text-center py-5">
        <button onClick={()=>this.props.giamSoLuong(-3)} className="btn btn-danger">-</button>
        <span className="display-4 text-primary">
          {this.props.number1}
        </span>
        <button onClick={this.props.tangSoLuong} className="btn btn-success">+</button>
        <p> {this.props.isLogin}</p> 
        <button onClick={this.props.tangSo2} className="btn btn-warning">+</button>

      </div>
    );
  }
}

//lấy dữ liệu về dưới dạng props
let mapStateToProps = (state2) => {
  return {
    number1: state2.number.soLuong,
    isLogin: state2.number.isLogin,
  };
};

//đẩy dữ liệu lên Root Reducer(store tổng)
let mapDispatchToProps = (dispatch3) => {
  return {
    tangSoLuong: () => {
      dispatch3(tangSoLuongAction());
    },
    giamSoLuong: (value) => {
        let action = {
          type: GIAM_SO_LUONG,
          payload: value,
        };
        dispatch3(action);
      },
    tangSo2: () => { dispatch3({
      type: 'cong5'
    }) }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DemoReduxMini);
