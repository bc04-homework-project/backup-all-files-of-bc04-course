import React, { Component } from "react";

export default class GioHang extends Component {
  renderTbody = () => {
    return this.props.gioHang.map((item) => {
      return (
        <tr>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img src={item.image} style={{ width: 80 }} />
          </td>
          <td>
            <button onClick={()=>this.props.handleSoLuong(item.id, false)} className="btn btn-warning"> - </button>
            <span className="mx-3">{item.soLuong}</span>
            <button onClick={()=>this.props.handleSoLuong(item.id, true)} className="btn btn-success"> + </button>
          </td>
          <td>{item.price*item.soLuong}</td>
          <td>
            <button onClick={()=>this.props.handleDelete(item)}handleDelete className="btn btn-danger"> Xóa </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="container py-5">
        <table className="table">
          <thead>
            <tr>
              <th>Tên</th>
              <th>Giá</th>
              <th>Hình ảnh</th>
              <th>Số lượng</th>
              <th>Thành tiền</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
          <tfoot>
            <tr>
              <td colSpan={3} className=""></td>
              <td>Tổng tiền</td>
              <td>0</td>
            </tr>
          </tfoot>
        </table>

        {this.props.gioHang.length == 0 && (
          <p>Chưa có sản phẩm trong giỏ hàng</p>
        )}
      </div>
    );
  }
}
