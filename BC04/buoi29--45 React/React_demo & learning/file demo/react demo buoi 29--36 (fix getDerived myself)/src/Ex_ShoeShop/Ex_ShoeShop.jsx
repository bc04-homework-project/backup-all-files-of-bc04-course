import React, { Component } from "react";
import { dataShoe } from "./Data_Shoe";
import ListShoe from "./ListShoe";
import GioHang from "./GioHang";
import DetailShoe from "./DetailShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detailShoe: "",
    gioHang: [],
  };

  handleChangeDetail = (index) => {
    this.setState({
      detailShoe: dataShoe[index],
    });
  };
  handleChangeDetail2 = (shoe) => {
    this.setState({
      detailShoe: shoe,
    });
  };
  handleChangeDetail3 = (idShoe) => {
    let index = this.state.shoeArr.findIndex((item) => {
      return item.id == idShoe;
    });
    index !== -1 &&
      this.setState({
        detailShoe: this.state.shoeArr[index],
      });
  };

  handleAddToCart = (shoe) => {
    let cloneGioHang = [...this.state.gioHang];

    let index = this.state.gioHang.findIndex((item) => {
      return item.id == shoe.id;
    });

    // case 1 - sản phẩm chưa có trong giỏ hàng
    if (index == -1) {
      let spGioHang = { ...shoe, soLuong: 1 };
      // console.log("spGioHang: ", spGioHang);
      cloneGioHang.push(spGioHang);
    } // case 2 - sản phẩm đã có trong giỏ hàng
    else {
      // let sp = this.state.gioHang[index];
      // sp.soLuong++;
      cloneGioHang[index].soLuong++;
    }

    this.setState(
      {
        gioHang: cloneGioHang,
      }
      // () => {
      //     console.log(this.state);
      //   }
    );
  };

  handleSoLuong = (shoeID, value) => {
    let index = this.state.gioHang.findIndex((shoe) => {
      return shoe.id == shoeID;
    });

    let cloneGioHang = [...this.state.gioHang];

    if (value) {
      cloneGioHang[index].soLuong++;
    } else {
      if (cloneGioHang[index].soLuong > 1) {
        cloneGioHang[index].soLuong--;
      }
    }

    this.setState({
      gioHang: cloneGioHang,
    });
  };

  handleDelete = (shoe) => {
    let index = this.state.gioHang.indexOf(shoe);
    let cloneGioHang = [...this.state.gioHang];
    cloneGioHang.splice(index, 1);

    this.setState({
      gioHang: cloneGioHang,
    });
  };
  
  render() {
    // console.log(this.state.gioHang.length);
    return (
      <div>
        <p>Ex_ShoeShop</p>

        <GioHang
          handleSoLuong={this.handleSoLuong}
          handleDelete={this.handleDelete}
          gioHang={this.state.gioHang}
        />
        <ListShoe
          data={this.state.shoeArr}
          handleChangeDetail={this.handleChangeDetail}
          handleChangeDetail2={this.handleChangeDetail2}
          handleChangeDetail3={this.handleChangeDetail3}
          handleAddToCart={this.handleAddToCart}
        />
        <DetailShoe detailShoe={this.state.detailShoe} />
      </div>
    );
  }
}
