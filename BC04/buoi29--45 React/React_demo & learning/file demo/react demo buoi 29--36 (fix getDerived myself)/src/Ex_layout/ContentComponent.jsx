import React, { Component } from 'react'

export default class ContentComponent extends Component {
  render() {
    return (
      <div 
      style={{
        height: 200,
      }}
      className='bg-info text-white text-left text-center'>ContentComponent</div>
    )
  }
}
