import React, { Component } from "react";

export default class Demo_State extends Component {
  //biến nằm trong CLASS thì khai báo không cần var/let/const, đi kèm dấu bằng
  state = {
    //biến nằm trong 1 biến kiểu object thì khai báo như cặp key-value, đi kèm dấu 2 chấm
    isLogin: false,
    username: "Alice",
    soLuong: 1,
  };

  handeLogin = (username) => {
    console.log('Hi', username);
    this.setState({
      isLogin: true,
    });
  };
  handeLogout = () => {
    this.setState({
      isLogin: false,
    });
  };

  handlePlus = () => {
    this.setState({
      soLuong: this.state.soLuong + 5,
    });
  };
  handleMinus = () => {
    this.setState({
      soLuong: this.state.soLuong - 1,
    });
  };

  render() {
    return (
      <div>
        <p>Demo_State</p>

        {this.state.isLogin ? (
          <button onClick={this.handeLogout} className="btn btn-primary">
            Logout
          </button>
        ) : (
          <div>
          <button onClick={() =>{this.handeLogin(this.state.username)}} className="btn btn-warning">
            Login
          </button>
          <p className="m-3">Hello {this.state.username}</p>
          </div>

        )}


        <p>
          <button onClick={this.handlePlus} className="btn btn-primary">
            +
          </button>
          <span className="m-5">{this.state.soLuong}</span>
          <button onClick={this.handleMinus} className="btn btn-warning">
            -
          </button>
        </p>
      </div>
    );
  }
}
