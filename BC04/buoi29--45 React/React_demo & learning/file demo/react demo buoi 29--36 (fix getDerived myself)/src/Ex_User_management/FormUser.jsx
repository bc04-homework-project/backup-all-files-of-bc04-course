import { nanoid } from "nanoid";
import React, { Component } from "react";
import { initialUserForm } from "./util";

export default class FormUser extends Component {
  state = {
    user: initialUserForm,
  };

  // UNSAFE_componentWillReceiveProps(nextProps, state){
  //   console.log('nextProps :', nextProps, 'state: ',  state);
  //   if (nextProps.userEdited) {
  //     this.setState({
  //       user: nextProps.userEdited
  //     })
  //   }
  // }

  static getDerivedStateFromProps(newProps, prevState) {
    // console.log('newProps :', newProps,'prevState: ', prevState);
    if (newProps.userEdited) {
      return { user: newProps.userEdited };
    } else {
      return null;
    }
  }

  handleChangeForm = (event) => {
    // let name = event.target.name;
    // console.log("name: ", name);
    // console.log("event: ", event.target.value);
    // let value = event.target.value;
    // console.log(event);

    this.props.handleUserEditFalse(); //set như vầy mới chỉnh được form và ngăn hàm getDerivedState chạy khi localState thay đổi và rerender

    let { name, value } = event.target;
    // console.log('name, value: ', name, value);
    this.setState({
      user: { ...this.state.user, [name]: value },
      // isEdit: false,
    });
  };

  handleAddUser = () => {
    let newUser = { ...this.state.user, id: nanoid() };
    // newUser.id = nanoid();
    // this.setState({
    //   user: initialUserForm,
    // })
    this.props.handleUserAdd(newUser);
  };

  handleUpdateUser = () => {
    let updatedUser = { ...this.state.user };
    this.props.handleUserUpdate(updatedUser);

    // this.setState({
    //   user: initialUserForm,
    // })
  };

  render() {
    // console.log(this.state.user);
    return (
      <div>
        <form>
          <div className="form-group">
            <input
              onChange={(e) => {
                this.handleChangeForm(e);
              }}
              value={this.state.user.account}
              type="text"
              className="form-control"
              name="account"
              id
              aria-describedby="helpId"
              placeholder="Tài khoản"
            />
          </div>
          <div className="form-group">
            <input
              onChange={(e) => {
                this.handleChangeForm(e);
              }}
              value={this.state.user.userName}
              type="text"
              className="form-control"
              name="userName"
              id
              aria-describedby="helpId"
              placeholder="Họ tên"
            />
          </div>
          <div className="form-group">
            <input
              onChange={(e) => {
                this.handleChangeForm(e);
              }}
              value={this.state.user.email}
              type="text"
              className="form-control"
              name="email"
              id
              // aria-describedby="helpId"
              placeholder="Email"
            />
          </div>
          <button
            type="button"
            onClick={this.handleAddUser}
            className="btn btn-success"
          >
            Add User
          </button>
          <button
            type="button"
            onClick={this.handleUpdateUser}
            className="btn btn-warning"
          >
            Update User
          </button>
        </form>
      </div>
    );
  }
}
