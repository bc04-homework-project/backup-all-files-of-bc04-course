import React, { Component } from "react";
import Header from "./Header";

export default class Demo_LifeCycle extends Component {
  componentDidMount() {
    //componentDidmount chạy 1 lần duy nhất sau khi render() chạy lần đầu

    //gọi API ở đây
    console.log("App Didmount");
  }
  state = {
    like: 1,
    share: 1,
  };
  render() {
    console.log("App render");

    return (
      <div className="text-center">
        {this.state.like < 5 ? <Header like={this.state.like} /> : ""}
        <div>
          <span className="text-danger display-4">{this.state.like}</span>{" "}
          <br />
          <button
            className="btn btn-primary"
            onClick={() => this.setState({ like: this.state.like + 1 })}
          >
            Plus Like
          </button>
        </div>
        <div>
          <span className="text-danger display-4">{this.state.share}</span>{" "}
          <br />
          <button
            className="btn btn-primary"
            onClick={() => this.setState({ share: this.state.share + 1 })}
          >
            Plus Share
          </button>
        </div>
      </div>
    );
  }

  componentDidUpdate() {
    //componentDidupdate tự động chạy sau khi render() chạy
    console.log("Did update");
  }
}
