import React, { Component } from "react";

let cardStyle = { width: "18rem" }
export default class Data_Binding extends Component {
  //biến nằm trong CLASS thì khai báo không cần var/let/const, đi kèm dấu bằng
    title = "Vui hhénn";
  render() {
    //biến nằm trong hàm thì khai báo có var/let/const
    let imgSrc = "https://picsum.photos/200"
    return (
      <div>
        <div className="card" style={cardStyle}>
          <img
            className="card-img-top"
            src={imgSrc}
            alt="Card image cap"
          />
          <div className="card-body">
            <h5 className="card-title">{this. title}</h5>
            <p className="card-text">{this. title}
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary">
              Go somewhere
            </a>
          </div>
        </div>
      </div>
    );
  }
}
