import React, { Component } from "react";
import { connect } from "react-redux";
import { PLAY_GAME, Tai } from "./xucxacContstants/xucxacConstants";

class KetQua extends Component {
  render() {
    let { luaChon, soBanChoi, soBanThang } = this.props.xucxac;
    return (
      <div className="text-center pt-5 display-4">
        <button
          onClick={this.props.handlePlayGame}
          class="btn btn-primary display-1"
        >
          <span class="display-4">Play Game</span>
        </button>
        <p className={luaChon == Tai ? "text-danger" : "text-success"}>
          Bạn chọn : {luaChon}
        </p>
        <p>
          Số bàn thắng : <span className="text-danger"> {soBanThang}</span>
        </p>
        <p>
          Số lượt chơi : <span className="text-warning"> {soBanChoi}</span>
        </p>
      </div>
    );
  }

  // render() {
  //   return (
  //     <div className='text-center pt-5 display-4'>
  //       <button onClick={this.props.handlePlayGame} class="btn btn-primary display-1">
  //       <span class="display-4">Play Game</span>
  //          </button>
  //          <p className={this.props.luaChon==Tai?'text-danger':'text-success'}>Bạn chọn : {this.props.luaChon}</p>
  //          <p>Số bàn thắng : <span className='text-danger'> {this.props.soBanThang}</span></p>
  //          <p>Số lượt chơi : <span className='text-warning'> {this.props.soLuotChoi}</span></p>
  //     </div>
  //   )
  // }
}

const mapStateToProps = (state) => ({
  xucxac: state.xucxacReducer,
  // luaChon: state.xucxacReducer.luaChon,
  // soLuotChoi: state.xucxacReducer.soBanChoi,
  // soBanThang: state.xucxacReducer.soBanThang,
});

const mapDispatchToProps = (dispatch) => {
  return {
    handlePlayGame: () => {
      dispatch({
        type: PLAY_GAME,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(KetQua);
