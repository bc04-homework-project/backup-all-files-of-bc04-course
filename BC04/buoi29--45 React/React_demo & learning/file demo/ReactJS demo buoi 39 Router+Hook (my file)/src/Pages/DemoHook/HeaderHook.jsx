import React, { memo, useEffect } from "react";

function HeaderHook({ like, handleLikePlus }) {
  useEffect(() => {
    console.log("useEffect header renderr");
  }, []);


  console.log("Header renderr");

  return (
    <div style={{ height: "max-content" }} className="bg-warning mb-5">
      <p>Header Hook</p>

      <p>Like : {like}</p>
      <button onClick={handleLikePlus}> Plus Like </button>
    </div>
  );
}

export default memo(HeaderHook);
//memo ~ PureComponent