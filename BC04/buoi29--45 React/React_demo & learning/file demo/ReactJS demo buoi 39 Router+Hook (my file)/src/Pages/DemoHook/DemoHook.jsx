import React, { useCallback, useState } from "react";
import { useEffect } from "react";
import HeaderHook from "./HeaderHook";

export default function DemoHook() {
  let [like, setLike2] = useState(100);
  // console.log('useState: ', useState);
  let [share, setShare2] = useState(100);

  let handleSharePlus = () => {
    setShare2(share + 1);
  };

  let handleLikePlus = useCallback( () => {
    setLike2(like + 1);
  }, [like]);

  //life cycle
  useEffect(() => {
    // call axios here
    // giống componentDidmount
    console.log("DemoHook useEffect renderr");
  }, [share]);

  console.log("DemoHook renderr");

  return (
    <div className="text-center display-4">
      <HeaderHook like={like} handleLikePlus={handleLikePlus} />
      <span className="text-success m-5">{like}</span>
      <button onClick={handleLikePlus} className="btn btn-success">
        Plus Like
      </button>
<br />
      <span className="text-primary m-5">{share}</span>
      <button onClick={handleSharePlus} className="btn btn-primary">
        Plus Share
      </button>
    </div>
  );
}
