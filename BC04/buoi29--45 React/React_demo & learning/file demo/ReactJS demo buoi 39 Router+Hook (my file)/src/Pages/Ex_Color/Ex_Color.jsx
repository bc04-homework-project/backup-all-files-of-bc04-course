import React from 'react'
import { useState } from 'react';
import { colors } from '../../utils/utils'



export default function Ex_Color() {
    let n = 1;

    const [color, setColor] = useState(colors[0]);

    let handleSetColor = (colorChange) => { 
        setColor(colorChange)
     };


    let renderButtonColor = () => { 
       return colors.map(color => {
            return <button onClick={()=>handleSetColor(color)} style={{backgroundColor: color}} className='btn d-block my-5'>color {n++}</button>
        });
     };


  return (
    <div><p>Ex_Color</p>
    
    <div className=" row">
        <div style={{backgroundColor: color, aspectratio: '1/1'}} className="col-6"></div>

        <div className="col-6">
            {renderButtonColor()}

        </div>



    </div>
    
    </div>
  )
}
