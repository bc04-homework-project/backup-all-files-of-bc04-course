import axios from "axios";
import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { hideLongString, Token_CyberSoft } from "../../utils/utils";

export default class HomePage extends Component {
  state = {
    movieList: [],
  };

  componentDidMount() {
    //check user đã đăng nhập hay chưa
    let userJson = localStorage.getItem("User");
    if (!JSON.parse(userJson)) {
      //chuyển hướng về trang login nếu user chưa đăng nhập
      window.location.href = "/login";
    }

    axios({
      url: `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP13`,
      method: "GET",
      headers: {
        TokenCybersoft: Token_CyberSoft,
      },
    })
      .then((res) => {
        // console.log(res);
        this.setState({ movieList: res.data.content });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  renderContent = () => {
    if (this.state.movieList.length == 0) {
      return (
        <div className="display-4 col-12 text-center text-danger">
          There is no data to show
        </div>
      );
    }

    return this.state.movieList.map((movie) => {
      return (
        <div className="card col-3" style={{ width: "18rem" }}>
          <img
            className="card-img-top"
            src={movie.hinhAnh}
            alt="Card image cap"
          />
          <div className="card-body">
            <h5 className="card-title">{movie.tenPhim}</h5>
            <p className="card-text">{hideLongString(movie.moTa, 30)}...</p>
            <NavLink to={`/detail/${movie.maPhim}`} className="btn btn-primary">
              Xem chi tiết
            </NavLink>
          </div>
        </div>
      );
    });
  };

  render() {
    return (
      <div>
        <p>HomePage</p>
        <div className="container row">{this.renderContent()}</div>
      </div>
    );
  }
}
