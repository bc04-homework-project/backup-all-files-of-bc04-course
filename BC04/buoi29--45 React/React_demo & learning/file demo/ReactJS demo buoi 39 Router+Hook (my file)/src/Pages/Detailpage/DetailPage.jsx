import axios from "axios";
import React, { Component } from "react";
import { Token_CyberSoft } from "../../utils/utils";

export default class DetailPage extends Component {
  state = {
    detail: null,
  };

  componentDidMount() {
    let { id } = this.props.match.params;

    axios({
      url: `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}
     `,
      method: "GET",
      headers: {
        TokenCybersoft: Token_CyberSoft,
      },
    })
      .then((res) => {
        console.log(res);
        this.setState({ detail: res.data.content });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    let { detail } = this.state;
    return (
      <div className="container">
        {/* dấu ? => optional chaining */}
        <p className="display-4">{detail?.tenPhim}</p>
        <img className="" src={detail?.hinhAnh} style={{ height: 500 }} />
        <p className="">{detail?.moTa}</p>
      </div>
    );
  }
}
