import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

export default class Header extends Component {
  render() {
    return (
      <div>
        <NavLink className={'btn btn-primary mx-2'} to="/"> Home page</NavLink>
        <NavLink className={'btn btn-primary mx-2'} to="/detail"> Detail page</NavLink>
        <NavLink className={'btn btn-primary mx-2'} to="/login"> Login page</NavLink>
        <NavLink className={'btn btn-primary mx-2'} to="/Demo-Hook"> Demo Hook</NavLink>
        <NavLink className={'btn btn-primary mx-2'} to="/Ex-color"> Ex-color</NavLink>
      </div>
    )
  }
}
