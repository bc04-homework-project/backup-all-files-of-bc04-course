export const hideLongString = (description, length) => { 
    let lengthDescription = description.length;

    if (lengthDescription > length) {
      return description.slice(0, length)  
    } else {
        return description
    }
    ;
 };

 export const Token_CyberSoft = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNCIsIkhldEhhblN0cmluZyI6IjIwLzAyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3Njg1MTIwMDAwMCIsIm5iZiI6MTY1NDEwMjgwMCwiZXhwIjoxNjc2OTk4ODAwfQ.QYLXMgjth5hQh9opZbNS7JEDPZGWA3o_95kR_VyLix8";

 
export const colors = ['#90f1ef', '#ffd6e0', '#ffef9f', '#c1fba4', '#7bf1a8']