import logo from './logo.svg';
import './App.css';
import 'antd/dist/antd.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import LoginPage from './Pages/LoginPage/LoginPage';
import HomePage from './Pages/HomePage/HomePage';
import DetailPage from './Pages/Detailpage/DetailPage';
import Header from './Components/Header/Header';
import DemoHook from './Pages/DemoHook/DemoHook';
import Ex_Color from './Pages/Ex_Color/Ex_Color';


function App() {
  return (
   <div className='container'>
   
   <BrowserRouter>

   <Header/>

   <Switch>
   <Route path="/" exact component={HomePage} />
   <Route path="/login" component={LoginPage} />
   <Route path="/detail/:id" component={DetailPage} />
   {/* <Route path="/detail/:id" render={()=> {return <DetailPage/>;}} /> */}

   <Route path="/Demo-Hook" component={DemoHook} />
   <Route path="/Ex-color" component={Ex_Color} />

   </Switch>

   </BrowserRouter>

   </div>
  );
}

export default App;
