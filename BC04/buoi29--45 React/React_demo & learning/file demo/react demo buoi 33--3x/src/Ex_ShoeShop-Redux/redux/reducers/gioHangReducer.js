import { addToCart } from "../constants/shoeConstant";

let stateGioHang = [];

export let gioHangReducer = (state = stateGioHang, { type, payload }) => {
  if (type == addToCart) {
    let index = state.indexOf(payload);
    let gioHang = [...state]; //thử lại không thay đổi trực tiếp state mà thông qua 1 biến trung gian
    if (index == -1) {
      let spGioHang = { ...payload, soLuong: 1 };
      console.log("spGioHang: ", spGioHang);
      gioHang.push(spGioHang);
      return [...state];
    } else {
      gioHang[index].soLuong++;
      return [...state];
    }
  } else {
    return [...state];
  }


  
  //   switch (type) {
  //     case addToCart: {
  //       let index = state.indexOf(payload);
  //       if (index == -1) {
  //         let spGioHang = { ...payload, soLuong: 1 };
  //         console.log("spGioHang: ", spGioHang);
  //         state.push(spGioHang);
  //         return [...state];
  //       } else {
  //         state[index].soLuong++;
  //         return [...state];
  //       }
  //     }

  //     default: {
  //       return [...state];
  //     }
  //   }
};
