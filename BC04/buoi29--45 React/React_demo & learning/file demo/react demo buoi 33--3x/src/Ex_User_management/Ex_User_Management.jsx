import React, { Component } from 'react'
import FormUser from './FormUser'
import TableUser from './TableUser'

export default class Ex_User_Management extends Component {
  render() {
    return (
      <div className='container py-5'>
        <FormUser/>
        <TableUser/>
      </div>
    )
  }
}
