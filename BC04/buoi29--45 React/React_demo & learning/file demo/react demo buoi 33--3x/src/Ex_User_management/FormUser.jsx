import React, { Component } from "react";
import { initialUserForm } from "./util";

export default class FormUser extends Component {
    state ={
        user: initialUserForm,
    }

    handleChangeUserName = (event) => { 
        console.log('event: ', event.target.value);
        let value = event.target.value;

        this.setState({
            user: {...this.state.user, userName: value}
        })
    
        
     }
  render() {
    console.log(this.state.user);
    return (
      <div>
        <div className="form-group">
          <input
          onChange={(e)=>{this.handleChangeUserName(e)}}
          value={this.state.user.account}
            type="text"
            className="form-control"
            name='account'
            id
            aria-describedby="helpId"
            placeholder="Tài khoản"
          />
        </div>
        <div className="form-group">
          <input
          onChange={(e)=>{this.handleChangeUserName(e)}}
          value={this.state.user.userName}
          type="text"
            className="form-control"
            name='userName'
            id
            aria-describedby="helpId"
            placeholder="Họ tên"
          />
        </div>
        <div className="form-group">
          <input
          value={this.state.user.email}
          type="text"
            className="form-control"
            name='email'
            id
            // aria-describedby="helpId"
            placeholder="Email"
          />
        </div>
      </div>
    );
  }
}
