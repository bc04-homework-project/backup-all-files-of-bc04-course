import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { name, image, price } = this.props.detail;
    let handleChangeDetail = this.props.handleChangeDetail;
    let index = this.props.index;
    let handleChangeDetail2 = this.props.handleChangeDetail2;
    let handleChangeDetail3 = this.props.handleChangeDetail3;

    return (
      <div>
        <div className="card" >
          <img className="card-img-top" src={image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary">
              ${price}
            </a>
            <button onClick={()=> this.props.handleAddToCart(this.props.detail)} className="btn btn-success">Add to cart</button>
            {/* <button onClick={()=> handleChangeDetail(index)} className='btn btn-warning'>Xem chi tiết</button> */}
            {/* <button onClick={()=> handleChangeDetail2(this.props.detail)} className='btn btn-warning'>Xem chi tiết</button> */}
            <button
              onClick={() => handleChangeDetail3(this.props.detail.id)}
              className="btn btn-warning"
            >
              Xem chi tiết
            </button>
          </div>
        </div>
      </div>
    );
  }
}
