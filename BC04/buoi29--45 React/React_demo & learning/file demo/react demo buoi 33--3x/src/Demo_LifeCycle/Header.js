import React, { PureComponent } from "react";

export default class Header extends PureComponent {
  componentDidMount() {
    this.myInterval = setInterval(() => {
      console.log("okkk");
    }, 1000);
  }
  
  shouldComponentUpdate(nextProps, nextState) {
    // console.log('nextProps: ', nextProps);
    if (nextProps.like == 3) {
      return false;
    }
    return true;
  }

  render() {
    console.log("Header render");
    return (
      <div className="py-5 bg-warning display-4">
        <p>Header</p>
        <p>like: {this.props.like}</p>
      </div>
    );
  }

  componentWillUnmount() {
    console.log("Header die");
    clearInterval(this.myInterval);
  }
}

//pure component ~ hạn chế render không cần thiết ~ pass by value ~ shallow compare
