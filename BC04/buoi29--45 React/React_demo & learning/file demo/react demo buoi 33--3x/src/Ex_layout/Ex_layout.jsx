import React, { Component } from 'react'
import ContentComponent from './ContentComponent'
import FooterComponent from './FooterComponent'
import HeaderComponent from './HeaderComponent'
import HomeComponent from './HomeComponent'
import NavigateComponent from './NavigateComponent'
import style from "./ex_layout.module.css"

export default class Ex_layout extends Component {
  render() {
    return (
      <div>
        <div className="container py-5">
          <p className={style["title-color"]}>Hello header</p>
          <p className={style.bgPrimary}>Demo background</p>
          <p className={`${style["title-color"]} ${style.bgPrimary}`}>Demo background 2</p>

            <HomeComponent/>
            <HeaderComponent/>
            <div className="navContent row">
            <div className='col-4'>   <NavigateComponent/></div>
            <div className='col-8'>   <ContentComponent/></div>  
            </div>
            <FooterComponent/>

          
          
        </div>
      </div>
    )
  }
}
