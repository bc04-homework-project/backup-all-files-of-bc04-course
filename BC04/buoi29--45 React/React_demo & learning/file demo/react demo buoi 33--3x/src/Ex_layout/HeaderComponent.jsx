import React, { Component } from 'react'

export default class HeaderComponent extends Component {
  render() {
    return (
      <div className="headerComponent bg-primary text-white">Header Component</div>
    )
  }
}
