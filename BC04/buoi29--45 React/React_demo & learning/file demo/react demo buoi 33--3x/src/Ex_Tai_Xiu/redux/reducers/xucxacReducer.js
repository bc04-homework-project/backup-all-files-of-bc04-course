import {
  LUA_CHON,
  PLAY_GAME,
  Tai,
  Xiu,
} from "../../xucxacContstants/xucxacConstants";

const initialState = {
  luaChon: "",
  soBanThang: 0,
  soBanChoi: 0,
  mangXucXac: [
    {
      img: "./xuc_xac/1.png",
      giaTri: 1,
    },
    {
      img: "./xuc_xac/1.png",
      giaTri: 1,
    },
    {
      img: "./xuc_xac/1.png",
      giaTri: 1,
    },
  ],
};

export let xucxacReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case PLAY_GAME: {
      let newMangXucXac = [];
      let score = 0;
      state.mangXucXac.forEach((item) => {
        let randomNum = Math.floor(Math.random() * 6 + 1);
        score += randomNum;
        let newXucXac = {
          img: `./xuc_xac/${randomNum}.png`,
          giaTri: randomNum,
        };
        newMangXucXac.push(newXucXac);
      });
      state.mangXucXac = newMangXucXac;
      state.soBanChoi++;
      score >= 11 && state.luaChon == Tai && state.soBanThang++;
      score < 11 && state.luaChon == Xiu && state.soBanThang++;

      return { ...state };
    }

    case LUA_CHON: {
      state.luaChon = payload;
      return { ...state };
    }

    default:
      return state;
  }
};
