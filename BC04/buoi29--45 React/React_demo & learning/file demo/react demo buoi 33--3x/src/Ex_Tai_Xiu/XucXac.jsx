import React, { Component } from "react";
import { connect } from "react-redux";
import { LUA_CHON, Tai, Xiu } from "./xucxacContstants/xucxacConstants";

let btnStyle = {
  width: 150,
  height: 150,
  fontSize: 44,
};
export class XucXac extends Component {
  render() {
    // console.log(this.props.mangXucXac);
    let { mangXucXac } = this.props;
    return (
      <div
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <button
          onClick={() => this.props.handleSelectOption(Tai)}
          style={btnStyle}
          class="btn btn-danger"
        >
          Tài
        </button>
        <div>
          {mangXucXac.map((item) => {
            return <img style={{ width: 100 }} src={item.img} />;
          })}
        </div>
        <button
          onClick={() => this.props.handleSelectOption(Xiu)}
          style={btnStyle}
          class="btn btn-success"
        >
          Xỉu
        </button>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  mangXucXac: state.xucxacReducer.mangXucXac,
});

const mapDispatchToProps = (dispatch) => {
  return {
    handleSelectOption: (option) => {
      dispatch({
        type: LUA_CHON,
        payload: option,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(XucXac);
