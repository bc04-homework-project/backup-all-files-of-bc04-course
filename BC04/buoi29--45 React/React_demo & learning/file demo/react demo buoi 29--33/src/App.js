import logo from './logo.svg';
import './App.css';
import DemoClass from './DemoComponent/DemoClass';
import DemoFunction from './DemoComponent/DemoFunction';
import Ex_layout from './Ex_layout/Ex_layout';
import Data_Binding from './Data_Binding/Data_Binding';
import Event_Binding from './Event_Binding/Event_Binding';
import Conditional_Rendering from './Conditional_Rendering/Conditional_Rendering';
import Demo_State from './Demo_State/Demo_State';
import RenderWithMap from './RenderWithMap/RenderWithMap';
import Demo_Prop from './Demo_Prop/Demo_Prop';
import Phone_info from './RenderWithMap/Phone_info';
import Ex_ShoeShop from './Ex_ShoeShop/Ex_ShoeShop';
import DemoReduxMini from './DemoReduxMini/DemoReduxMini';
import Ex_ShoeShop_Redux from './Ex_ShoeShop-Redux/Ex_ShoeShop';

function App() {
  return (
   <div className='App'>
    {/* <DemoClass/>
    <DemoClass/>
    <DemoClass/>

    <DemoFunction/> */}

    {/* <Ex_layout/> */}

    {/* <Data_Binding/> */}

    {/* <Event_Binding/> */}

    {/* <Conditional_Rendering/> */}

    {/* <Demo_State/> */}

    {/* <RenderWithMap/> */}
    {/* <Phone_info/> */}

    {/* <Demo_Prop/> */}

    {/* <Ex_ShoeShop/> */}

    {/* <DemoReduxMini/> */}

    <Ex_ShoeShop_Redux/>

    
   </div>
  );
}

export default App;
