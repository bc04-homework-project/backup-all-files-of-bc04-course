import React, { Component } from "react";
import RenderWithMap from "./RenderWithMap";

export default class Phone_info extends Component {
  state = {
    phoneArr: [
      {
        name: "Arthur Feeney",
        img: "http://loremflickr.com/640/480/people",
        price: "42.00",
        id: "1",
      },
      {
        name: "Blake Muller",
        img: "http://loremflickr.com/640/480/technics",
        price: "127.00",
        id: "2",
      },
      {
        name: "Sherry Gaylord Jr.",
        img: "http://loremflickr.com/640/480/nightlife",
        price: "190.00",
        id: "3",
      },
      {
        name: "Herbert Erdman Jr.",
        img: "http://loremflickr.com/640/480/nightlife",
        price: "540.00",
        id: "4",
      },
      {
        name: "Ella Rath",
        img: "http://loremflickr.com/640/480/people",
        price: "398.00",
        id: "5",
      },
      {
        name: "Bessie Strosin DDS",
        img: "http://loremflickr.com/640/480/food",
        price: "660.00",
        id: "6",
      },
      {
        name: "Owen Kihn",
        img: "http://loremflickr.com/640/480/sports",
        price: "635.00",
        id: "7",
      },
      {
        name: "Brittany Ullrich II",
        img: "http://loremflickr.com/640/480/fashion",
        price: "657.00",
        id: "8",
      },
      {
        name: "Jodi Spencer",
        img: "http://loremflickr.com/640/480/cats",
        price: "188.00",
        id: "9",
      },
      {
        name: "Byron Mayert II",
        img: "http://loremflickr.com/640/480/food",
        price: "697.00",
        id: "10",
      },
    ],
  };
  render() {
    return (
      <div>
        <p>Phone_info</p>

        <RenderWithMap phoneInfo={this.state.phoneArr} />
      </div>
    );
  }
}
