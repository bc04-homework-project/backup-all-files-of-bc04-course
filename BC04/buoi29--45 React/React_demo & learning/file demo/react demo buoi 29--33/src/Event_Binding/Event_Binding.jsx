import React, { Component } from 'react'

export default class Event_Binding extends Component {
    // function không có tham số
  handleClickMe =  () => {
    console.log('okkk');
  };
    // function có tham số
  handleSayHello = (name) => {
    console.log(name);
  }
  showSomething = (eto) =>{
    console.log(eto);
  }
  
  //trong code có HTML format thì cần có binding dữ liệu = ngoặc nhọn
    render() {
    return (
      <div><p>Event_Binding</p>
      
      <button onClick={this.handleClickMe} className='btn btn-primary'>Click Me</button>

      <button onClick={()=>{ this.handleSayHello('Aliceee')}} className='btn btn-success'>Click Me 2</button>

      <button onClick={()=>{ this.showSomething('Tommm')}} className='btn btn-warning'>Click Me 3</button>


      
      </div>
    )
  }
}
