import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          {this.props.dataShoe.map((item, index) => {
            return (
              <div className="col-3">
                <ItemShoe
                  detail={item}
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}


// let mapStateToProps = state => {
//   return {
//     dataShoe: state.shoeReducer.dataShoe
//   }
// }

let mapStateToProps = state => {
  return {
    dataShoe: state.shoeReducer
  }
}



export default connect(mapStateToProps)(ListShoe)