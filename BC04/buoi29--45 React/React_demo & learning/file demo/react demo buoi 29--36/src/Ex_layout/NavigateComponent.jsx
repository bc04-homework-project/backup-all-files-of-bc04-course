import React, { Component } from 'react'

export default class NavigateComponent extends Component {
  render() {
    return (
      <div
      style={{
        height: 200,
      }}
      className='bg-success text-white text-left'>Navigation Component</div>
    )
  }
}
