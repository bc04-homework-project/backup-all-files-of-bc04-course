import _ from "lodash";
import React, { Component } from "react";
import FormUser from "./FormUser";
import TableUser from "./TableUser";

export default class Ex_User_Management extends Component {
  state = {
    userList: [],
    userEdited: null,
  };

  handleUserAdd = (user) => {
    let cloneUserList = [...this.state.userList, user];
    this.setState(
      {
        userList: cloneUserList,
      },
      () => {
        console.log(this.state.userList);
      }
    );
  };

  handleUserDelete = (id) => {
    let index = _.findIndex(this.state.userList, (user) => user.id == id);
    console.log("index: ", index);

  };

  handleUserEdit = (id) => {
    let index = _.findIndex(this.state.userList, (user) => user.id == id);
    console.log("index: ", index);
    if (index == -1) return;

    let userEdited = this.state.userList[index];

    this.setState({ userEdited: userEdited, })

  }

  render() {
    return (
      <div className="container py-5">
        <FormUser handleUserAdd={this.handleUserAdd} userEdited={this.state.userEdited}/>
        <TableUser
          userList={this.state.userList}
          handleUserDelete={this.handleUserDelete}
          handleUserEdit={this.handleUserEdit}
        />
      </div>
    );
  }
}
