import { nanoid } from "nanoid";
import React, { Component } from "react";
import { initialUserForm } from "./util";

export default class FormUser extends Component {
  state = {
    user: initialUserForm,
    isEdit: false,
  };

  UNSAFE_componentWillReceiveProps(nextProps, state){
    console.log('nextProps, state: ', nextProps, state);
    if (nextProps.userEdited) {
      this.setState({
        user: nextProps.userEdited
      })
    }
  }

  // static getDerivedStateFromProps(newProps, state){
  //   console.log('newProps, state: ', newProps, state);

  // }

  // static getDerivedStateFromProps(newProps, state) {
  //   console.log("newProps, state: ", newProps, state);
  //   if(newProps.userEdited){
  //     return {
  //       user: newProps.userEdited,
  //     }
  //   }
  // }

  handleChangeForm = (event) => {
    // let name = event.target.name;
    // console.log("name: ", name);
    // console.log("event: ", event.target.value);
    // let value = event.target.value;
    // console.log(event);

    let { name, value } = event.target;
    // console.log('name, value: ', name, value);
    this.setState({
      user: { ...this.state.user, [name]: value },
    });
  };

  handleAddUser = () => {
    let newUser = { ...this.state.user };
    newUser.id = nanoid();
    this.setState({
      user: initialUserForm,
    })
    this.props.handleUserAdd(newUser);
  };

  render() {
    // console.log(this.state.user);
    return (
      <div>
        <form>
          <div className="form-group">
            <input
              onChange={(e) => {
                this.handleChangeForm(e);
              }}
              value={this.state.user.account}
              type="text"
              className="form-control"
              name="account"
              id
              aria-describedby="helpId"
              placeholder="Tài khoản"
            />
          </div>
          <div className="form-group">
            <input
              onChange={(e) => {
                this.handleChangeForm(e);
              }}
              value={this.state.user.userName}
              type="text"
              className="form-control"
              name="userName"
              id
              aria-describedby="helpId"
              placeholder="Họ tên"
            />
          </div>
          <div className="form-group">
            <input
              onChange={(e) => {
                this.handleChangeForm(e);
              }}
              value={this.state.user.email}
              type="text"
              className="form-control"
              name="email"
              id
              // aria-describedby="helpId"
              placeholder="Email"
            />
          </div>
          <button
            type="button"
            onClick={this.handleAddUser}
            className="btn btn-warning"
          >
            Add User
          </button>
        </form>
      </div>
    );
  }
}
