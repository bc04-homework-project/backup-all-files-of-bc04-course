import React, { Component } from "react";

export default class TableUser extends Component {
  renderTbody = () => {
    return this.props.userList.map((user) => {
      return (
        <tr>
          <td>{user.id}</td>
          <td>{user.account}</td>
          <td>{user.userName}</td>
          <td>{user.email}</td>
          <td>
          <button
              onClick={() => this.props.handleUserEdit(user.id)}
              className="btn btn-warning"
            >
              Edit
            </button>
            <button
              onClick={() => this.props.handleUserDelete(user.id)}
              className="btn btn-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Tài Khoản</th>
              <th>Họ Tên</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
