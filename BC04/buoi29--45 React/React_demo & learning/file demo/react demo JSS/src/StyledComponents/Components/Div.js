import styled from "styled-components";

export const Div = styled.div `
background : ${prop=> prop.theme.background};
color : ${prop=> prop.theme.color};
font-size: ${prop=> prop.theme.fontSize1};

`