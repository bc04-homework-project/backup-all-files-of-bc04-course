import styled from "styled-components";

export const Button2 = styled.button`
  color: blue;
  background-color: red;
  padding: 40px;
  cursor: pointer;
  // transition: 1s;
  &:hover {
    background: green;
    color: red;
    transition: color 0.5s;
  }
  &.style3 {
    background: pink;
    &:hover {
      background: gray;
    }
  }

  background: ${(prop3) => (prop3.primo3 ? "lightblue" : "orange")};
  color: ${(pro) => pro.text3 && "yellow"};

  background: ${(pro2) =>
    pro2.back1 ? "blue" : pro2.back2 ? "green" : pro2.back3 ? "purple" : ""};
`;

export const Button3 = styled(Button2)`
  background: yellow;
`;

export const Button4 = styled(Button2)`
  background: ${(pro) => pro.back4 || "rgb(68, 248, 113)"};
`;

export const Button5 = styled(Button2)`
  background: ${(prop) => prop.theme.background};
  color: ${(prop) => prop.theme.color};
  font-size: ${(prop) => prop.theme.fontSize1};
`;
