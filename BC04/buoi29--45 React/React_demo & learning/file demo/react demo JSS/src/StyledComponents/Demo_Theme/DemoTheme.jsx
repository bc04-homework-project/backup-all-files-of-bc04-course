import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import { Button5 } from "../Components/Button";
import { Div } from "../Components/Div";
import { darkTheme, lightTheme } from "./ThemeCompo";

export default class DemoTheme extends Component {
  state = {
    theme: lightTheme,
    dark: darkTheme,
    light: lightTheme,
  };

  handleChangeTheme = (event) => {
    this.setState({
      theme: event.target.value == 1 ? lightTheme : darkTheme,
    });
  };

  render() {
    return (
      <ThemeProvider theme={this.state.theme}>
        <div>
          <p>DemoTheme</p>

          <Div>Hellooo Worldddddd</Div>
          <Div>Hellooo Worldddddd</Div>

          <select
            onChange={this.handleChangeTheme}
            style={{ fontSize: "50px", padding: "20px" }}
          >
            <option value="1">Light</option>
            <option value="2">Dark</option>
          </select>
          <br />

          <Button5>Hooolleeee WWWoooolll</Button5>
        </div>
      </ThemeProvider>
    );
  }
}
