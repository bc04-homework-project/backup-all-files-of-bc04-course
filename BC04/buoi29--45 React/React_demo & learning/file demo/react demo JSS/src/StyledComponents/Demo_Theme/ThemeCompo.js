

export const darkTheme = {
    background: "black",
    color: 'white',
    fontSize1: '30px'
}

export const lightTheme = {
    background: "lightblue",
    color: 'black',
    fontSize1: '50px'
}
