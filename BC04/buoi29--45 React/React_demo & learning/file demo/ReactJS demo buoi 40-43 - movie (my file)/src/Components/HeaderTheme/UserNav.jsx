import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localServ } from "../../Pages/Services/localService";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  console.log("user: ", user);

  let handleLogout = () => { 
    //xóa data từ local storage
    localServ.user.remove();
    //remove data từ redux
   //dispatch 
    window.location.href = '/login'
   };

  let renderContent = () => {
    if (user) {
      return (
        <>
          <span className="font-medium text-blue-800 underline">
            {user.hoTen}
          </span>
          <button onClick={handleLogout} className="border rounded border-red-50 px-5 py-2 text-red-500">
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            {" "}
            <button className="border rounded border-black px-5 py-2 text-black hover:bg-black hover:text-white transition duration-300">
              Đăng nhập
            </button>
          </NavLink>
          <button className="border rounded border-black px-5 py-2 text-red-500">
            Đăng ký
          </button>
        </>
      );
    }
  };

  return <div className="space-x-5">{renderContent()}</div>;
}
