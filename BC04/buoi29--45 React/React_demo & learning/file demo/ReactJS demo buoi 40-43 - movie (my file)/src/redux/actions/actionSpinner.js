import { SET_LOADING_OFF, SET_LOADING_ON } from "../constants/constantSpinner";

export const setLoadingOn = () => ({
  type: SET_LOADING_ON,
});

export const setLoadingOff = () => ({
    type: SET_LOADING_OFF,
  });

