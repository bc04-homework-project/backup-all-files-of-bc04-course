import { localServ } from "../../Pages/Services/localService";
import { SET_USER } from "../constants/constantUser"

let initialState = {
    userInfo: localServ.user.get(),
}

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {

  case SET_USER:{
    state.userInfo = payload;
    return { ...state}
  }

  default:
    return state
  }
}
