import axios from "axios";
import { BASE_URL, https, TOKEN_CYBERSOFT } from "./configURL";


export const moviesServ = {
    // getListMovie: () => { 
    //     return axios ({
    //         url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP13`,
    //         method: 'GET',
    //         headers: {
    //             TokenCybersoft: TOKEN_CYBERSOFT
    //         }
    //     })
    //  },

    getListMovie: () => { 
        let uri = '/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP13'
        return https.get(uri)
    },

    // getMovieByTheatre: () => { 
    //     return axios({
    //         url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP13`,
    //         method: 'GET',
    //         headers: {
    //             TokenCybersoft: TOKEN_CYBERSOFT
    //         }
    //     })
    //  }

    getMovieByTheatre: () => { 
        let uri = '/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP13'
        return https.get(uri)
    }



};