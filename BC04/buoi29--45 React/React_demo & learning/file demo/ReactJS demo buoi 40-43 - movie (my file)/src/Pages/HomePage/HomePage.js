import { Col, Row } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Spinner from "../../Components/Spinner/Spinner";
import { setLoadingOff, setLoadingOn } from "../../redux/actions/actionSpinner";
import { moviesServ } from "../Services/moviesService";
import ItemMovie from "./ItemMovie";
import TabsMovies from "./TabsMovies";

export default function HomePage() {
  let dispatch = useDispatch();
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    dispatch(setLoadingOn());
    moviesServ
      .getListMovie()
      .then((res) => {
        console.log(res);
        setMovies(res.data.content);
        dispatch(setLoadingOff());
      })
      .catch((err) => {
        console.log(err);
        dispatch(setLoadingOff());
      });
  }, []);

  const renderMovies = () => {
    return movies.map((data, index) => {
      return <ItemMovie key={index} data={data} />;
    });
  };

  return (
    <div className="container mx-auto">
      <div className="grid grid-cols-5 gap-10 ">{renderMovies()}</div>

      <TabsMovies />
    </div>
  );
}
