import { Tabs } from "antd";
import React, { useEffect, useState } from "react";
import { moviesServ } from "../Services/moviesService";
import ItemTabsMovies from "./ItemTabsMovies";

export default function TabsMovies() {
  const [dataMovies, setDataMovies] = useState([]);
  useEffect(() => {
    moviesServ
      .getMovieByTheatre()
      .then((res) => {
        console.log(res);
        setDataMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderContent = () => {
    return dataMovies.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          tab={<img src={heThongRap.logo} className="w-16 h-16" />} key={index} >
          <Tabs
            style={{ height: 500 }}
            tabPosition={"left"}
            defaultActiveKey="1"
          >
            {heThongRap.lstCumRap.map((cumRap, index) => {
              return (
                <Tabs.TabPane
                  tab={
                    <div className="w-48 text-left">
                      <p className="text-gray-700 truncate">
                        {cumRap.tenCumRap}
                      </p>
                      <p className="truncate">{cumRap.diaChi}</p>
                  </div>
                  }
                  key={index}
                >
                  <div
                    style={{ height: 500, overflowY: "scroll" }}
                    className="h-32 scrollbar scrollbar-thumb-blue-700 scrollbar-track-blue-300 overflow-y-scroll hover:scrollbar-thumb-green-700 scrollbar-thumb-rounded-full scrollbar-track-rounded-full"
                  >
                    {cumRap.danhSachPhim.map((phim, index1) => {
                      return <ItemTabsMovies data={phim} key={index1}/>;
                    })}
                  </div>
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };

  return (
    <div>
      <Tabs style={{ height: 500 }} tabPosition={"left"} defaultActiveKey="1">
        {renderContent()}
      </Tabs>
    </div>
  );
}
