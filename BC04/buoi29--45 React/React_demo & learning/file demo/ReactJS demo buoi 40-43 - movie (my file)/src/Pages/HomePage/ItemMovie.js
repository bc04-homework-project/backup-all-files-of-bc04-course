import React from "react";
import { Card } from "antd";
import Meta from "antd/lib/card/Meta";
import { NavLink } from "react-router-dom";

export default function ItemMovie({ data }) {
  return (
    <div >
      <Card
        hoverable
        key={data.maPhim}
        style={{ width: '100%' }}
        cover={<img className="h-80 w-full object-cover" alt="example" src={data.hinhAnh} />}
      >
        <Meta title={<p className="truncate text-red-500">{data.tenPhim}</p>} description={<p className="truncate">{data.moTa}</p>} />
        <NavLink to={`/detail/${data.maPhim}`}> <button className="w-full py-2 text-center bg-red-500 text-white mt-5 rounded transition duration-300 hover:bg-blue-600">Xem chi tiết</button></NavLink>
      </Card>
    </div>
  );
}
