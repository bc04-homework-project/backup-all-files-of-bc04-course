import moment from "moment/moment";
import React from "react";

export default function ItemTabsMovies({ data }) {
  return (
    <div className="p-3 flex space-x-5 border-b border-b-red-500">
      <img className="w-28 h-40" src={data.hinhAnh} alt="" />
      <div className="flex-grow">
        <p>{data.tenPhim}</p>
        <div className="grid grid-cols-3 gap-5">
          {data.lstLichChieuTheoPhim.map((gioChieu, i) => {
            return (
              <div
                className="p-2 rounded bg-red-700 text-white text-center"
                key={i}
              >
                {moment(gioChieu.ngayChieuGioChieu).format(
                  "DD-MM-YYYY ~ hh:mm"
                )}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
