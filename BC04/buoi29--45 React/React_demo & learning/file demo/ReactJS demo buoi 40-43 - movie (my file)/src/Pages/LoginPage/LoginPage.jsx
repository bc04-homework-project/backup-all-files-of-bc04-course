import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userServ } from "../Services/userService";
import { localServ } from "../Services/localService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SET_USER } from "../../redux/constants/constantUser";
import Lottie from "lottie-react";
import bg_animate from "../../assets/bg_login.json";
import { setUserLoginActionServ } from "../../redux/actions/actionUser";

export default function LoginPage() {
  let navigate = useNavigate();

  let dispatch = useDispatch();

  const onFinish = (values) => {
    // userServ
    //   .postLogin(values)
    //   .then((res) => {
    //     //lưu vào cocal storage
    //     localServ.user.set(res.data.content);
    //     //dispatch to store
    //     dispatch({
    //       type: SET_USER,
    //       payload: res.data.content,
    //     });
    //     //chuyển hướng sang trang chủ
    //     message.success("Đăng nhập thành công");
    //     setTimeout(() => {
    //       navigate("/");
    //     }, 2000);
    //     console.log(res);
    //   })
    //   .catch((err) => {
    //     message.error("Đăng nhập thất bại");
    //     console.log(err);
    //   });

    let onSuccess = () => {
      message.success("Đăng nhập thành công");
      setTimeout(() => {
        navigate("/");
      }, 2000);
    };
    let onFail = () => {
      message.error("Đăng nhập thất bại");
    };

    dispatch(setUserLoginActionServ(values, onSuccess, onFail));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="container mx-auto h-screen w-screen flex items-center justify-center">
      <div className="w-1/2 h-full">
        <Lottie animationData={bg_animate} />
      </div>

      <div className="w-1/2 h-full flex items-center justify-center">
        <Form
          className=" w-full"
          layout="vertical"
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 24,
          }}
          initialValues={{}}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Username"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Password"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 24,
            }}
          >
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>{" "}
      </div>
    </div>
  );
}

/**
 * acc to test - 250196
 */
