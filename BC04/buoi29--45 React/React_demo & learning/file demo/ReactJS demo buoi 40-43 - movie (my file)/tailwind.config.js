/** @type {import('tailwindcss').Config} */
module.exports = {
  // important: true, //all TailWind CSS will be important

  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {},
  },
  plugins: [require("tailwind-scrollbar")],
  variants: {
    scrollbar: ["rounded"]
  }
};
