var a, b, c;

a = 10;
a+= a;
console.log("a:",a);
//a = 20

b = ++a + 5; //dấu cộng cộng phía trước thì ưu tiên tăng giá trị a trước
//=> b = 26

c = a++ + 5; //dấu cộng cộng phía sau thì ưu tiên phép tính tổng trước
//=> c = 26

console.log("a:", a);
//=> a = 22

a = 0;
console.log("b:", b);
console.log("c:", c);
// b and c  vẫn = 26 do mỗi dòng lệnh JS chỉ chạy 1 lần, không cập nhật khi có thay đổi phía dưới như HTML hay CSS


