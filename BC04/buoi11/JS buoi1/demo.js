console.log ("hello");


//variable

// var username = "Bob";

// hoisting
// ES5
// console.log(username);

//pass by value : string, number, boolean

// string
var username = 'Alice';

// number
var age = 2;


// boolean: true/false

// camel case
var isMarried = false;

// snake case
var is_married = false;

is_married = true;

var luckyNumber = null;

luckyNumber = 6897


const totalHolidays = 14;

totalHolidays = 20
console.log('totalHolidays: ', totalHolidays);

)(((((((({{{{{{}}}}}}))))))))