

console.log('hello world');

var a = 3;
var b = 4;
a = b--;

b = (--b) + (++a);
console.log('b: ', b);
console.log('a: ', a);

console.log('next 2');

var a = 5;
var b = 6;
a = (b++) + 3;
console.log('a: ', a);
b = (--b) + (++a);
console.log('b: ', b);
c = 2 * a + (++b);
console.log('c: ', c);
b = 2 * (++c) - (a++);
console.log('b: ', b);
console.log('a: ', a);
console.log('c: ', c);

console.log('next 3');

var a = 3;
var b = 5;
var c = 14.1;
// var sum;
sum = a + b + c;
console.log('sum: ', sum);
c /= a;
console.log('c: ', c);
b += c - a;
console.log('b: ', b);
a *= 2 + b + c;
console.log('a: ', a);

console.log('next 4');

var a = 3;
var b = 1;
a -= b;
console.log('a: ', a);
b = (b--) + (--a);
console.log('b: ', b);
console.log('a: ', a);









