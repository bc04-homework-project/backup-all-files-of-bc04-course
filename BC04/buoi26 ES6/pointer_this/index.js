window.name = 'global';

var person = {
    name : 'Jason',

    shout: function () {
        console.log('name 1', this.name);
        
    },

    shout2: () => {
        console.log('name 2', this.name);
    },

// shorter syntax       
    shout3(){
        console.log('name 3', this.name);
    }
}

person.shout();
person.shout2();
person.shout3();