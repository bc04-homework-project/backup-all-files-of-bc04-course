console.log('age');//not an error
var age = 3;
//var - function scope


console.log('num');// error
let num = 5;
//let - block scope


function checkLogin() {
    var isLogin = true;
    if (isLogin) {
        var timeLogin = null;
        timeLogin = 'Today';
    }
  console.log('timeLogin: ', timeLogin); // not an error - function scope
}

function checkLogin2() {
    let isLogin = true;
    if (isLogin) {
        let timeLogin = null;
        timeLogin = 'Today';
    }
  console.log('timeLogin: ', timeLogin); // error - block scope
}