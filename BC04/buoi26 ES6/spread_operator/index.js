let dog1 = {
    name: 'Bull',
    age: 2,
};

// let dog2 = dog1;
let dog2 = {...dog1, name:'Alice'};


// dog2.name = 'Alice';
console.log('dog1.name: ', dog1.name);

let dogArr = [dog1, dog2];
let dog3 = {
    name: 'Tom',
    age: 2,
};
let dogArr2 = [...dogArr, dog3]

