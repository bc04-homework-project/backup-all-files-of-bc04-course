const BASE_URL = "https://62db6cb7e56f6d82a772862e.mockapi.io/";

function getDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      // console.log(res);
      tatLoading();
      renderDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
getDSSV();

function xoaSinhVien(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      getDSSV();
      tatLoading();
    })
    .catch(function (err) {
      console.log(err);
      tatLoading();
    });
}

function themSV() {
  //newSV = object lấy từ form, dưới đây là hard code data làm ví dụ
  let newSV = {
    name: "Bánh Kẹo",
    email: "Lillie_Armstrong@hotmail.com",
    password: "UmGSrgfjJf6pyN1",
    math: 28605,
    physics: 5808,
    chemistry: 88300,
    id: "15", //id không cần thêm vì là random từ server
  };
  batLoading();

  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      console.log(res);
      getDSSV();
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
