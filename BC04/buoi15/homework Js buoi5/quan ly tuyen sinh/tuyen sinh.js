// vì các biến của 'khu vực' hay 'đối tượng' không có tương quan về giá trị như bài tính tiền Uber/Grab nên việc tách hàm có vẻ không phù hợp và sẽ làm code dài dòng hơn cần thiết

function xemKetQua() {
  var diem1 = document.getElementById("diemThi1").value * 1;
  var diem2 = document.getElementById("diemThi2").value * 1;
  var diem3 = document.getElementById("diemThi3").value * 1;
  //   console.log({ diem1, diem2, diem3 });

  var khuVuc = document.querySelector('input[id="zone"]:checked').value * 1;
  var doiTuong = document.getElementById("doiTuong").value * 1;
  var diemChuan = document.getElementById("diemChuan").value * 1;
  //   console.log({ khuVuc, doiTuong, diemChuan });

  var tongDiem = diem1 + diem2 + diem3 + khuVuc + doiTuong;
  var result;

  if (diem1 == 0 || diem2 == 0 || diem3 == 0) {
    result = "Bạn đã rớt";
  } else if (tongDiem >= diemChuan) {
    result = "Bạn đã đậu";
  } else {
    result = "Bạn đã rớt";
  }
  //   console.log("result: ", result);

  document.getElementById(
    "result"
  ).innerHTML = `<span class="text-danger">${result} . </span> Tổng điểm :  <span class="text-success">${tongDiem}</span>`;
}

