// tính tiền điện giống bài tính tiền khuyến mãi mua hàng shop, nên việc tách hàm là không phù hợp do chỉ có 1 biến về 'số lượng mua'

function tinhTien() {
  var tenKhach = document.getElementById("name").value;
  var soKwTieuThu = document.getElementById("soDienTieuThu").value * 1;

  var giaDien50kwDau = 500;
  var giaDien50_100kw = 650;
  var giaDien100_200kw = 850;
  var giaDien200_350kw = 1100;
  var giaDienSau350kw = 1300;

  var result = 0;

  if (soKwTieuThu <= 50) {
    result = giaDien50kwDau * soKwTieuThu;
  } else if (soKwTieuThu <= 100) {
    result = giaDien50kwDau * 50 + (soKwTieuThu - 50) * giaDien50_100kw;
  } else if (soKwTieuThu <= 200) {
    result =
      giaDien50kwDau * 50 +
      giaDien50_100kw * 50 +
      (soKwTieuThu - 100) * giaDien100_200kw;
  } else if (soKwTieuThu <= 350) {
    result =
      giaDien50kwDau * 50 +
      giaDien50_100kw * 50 +
      giaDien100_200kw * 100 +
      (soKwTieuThu - 200) * giaDien200_350kw;
  } else {
    result =
      giaDien50kwDau * 50 +
      giaDien50_100kw * 50 +
      giaDien100_200kw * 100 +
      giaDien200_350kw * 150 +
      (soKwTieuThu - 350) * giaDienSau350kw;
  }

//   console.log("result: ", result);

  document.getElementById("tenKhach").innerHTML = tenKhach;

  document.getElementById("result").innerHTML = result;
}
