function sayHello (){
    var msg = "Hello, World!";
    console.log(msg);
    console.log('have a good day');
}

// gọi hàm (không cần bấm nút onclick)
// sayHello();
sayHello();


// Hàm có tham số

function sayHelloByName(user){
    console.log('Hello ' + user);

}

sayHelloByName('Alice')
sayHelloByName('Bob')
sayHelloByName('1 + 2')
sayHelloByName(1 + 3)


// gọi hàm với biến
// vd 1
var diemToan = 2;
var diemVan = 2.5;

function tinhDtb(dtb){
    console.log('diem tb ', dtb);
}

tinhDtb((diemToan + diemVan)/2);

// vd 2
var diemToan = 3;
var diemVan = 4.5;
// log dtb=3,75

// Không có giá trị return
function tinhDiemTB2(diemToan, diemVan){
    var dtb = (diemToan + diemVan) / 2;
    console.log('dtb2: ', dtb);
}

tinhDiemTB2(6,8);
// log dtb=7


// vd 3

function tinhDiemTB3(diemToan, diemVan){
    var dtb = (diemToan + diemVan) / 2;
    console.log('dtb3: ', dtb);
    return dtb;
    console.log('okk');
    // sau lệnh 'return' hàm ngừng lại, chương trình không chạy tiếp
}

tinhDiemTB3(diemToan, diemVan);

var diemtTB = tinhDiemTB3(5, 8);
console.log('diemtTB: ', diemtTB);


