function dienTich() {
  var length = document.getElementById("length").value * 1;
  var width = document.getElementById("width").value * 1;

  var dienTich = length * width;

  document.getElementById(
    "dienTich"
  ).innerHTML = `Diện tích hình chữ nhật = ${dienTich}`;
}

function chuVi() {
  var length = document.getElementById("length").value * 1;
  var width = document.getElementById("width").value * 1;

  var chuVi = (length + width) * 2;

  document.getElementById(
    "chuVi"
  ).innerHTML = `Chu vi hình chữ nhật = ${chuVi}`;
}
