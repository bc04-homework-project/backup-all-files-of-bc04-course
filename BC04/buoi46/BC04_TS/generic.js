// const [like, setLike] = useState(1)
function useState() {
    var state;
    function getState() {
        return state;
    }
    function setState(x) {
        state = x;
    }
    return { getState: getState, setState: setState };
}
var like_useState = useState();
like_useState.setState(2);
console.log("like_useState: ", like_useState.getState());
function useState_generic() {
    var state;
    function getState() {
        return state;
    }
    function setState(x) {
        state = x;
    }
    return { getState: getState, setState: setState };
}
var num_useState = useState_generic();
num_useState.setState(3);
