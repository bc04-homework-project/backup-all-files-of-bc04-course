console.log("ookkk");

var username = "Alice";

username = "true";
//ngầm định kiểu dữ liệu

// typescript = type + javascript

// type: loại dữ liệu

// variable:type = value
//Primitive value
var account: string = "admin123";
let userAge: number = 2;
userAge = 3;
// userAge = "3";

let isHoliday: boolean = true;
isHoliday = false;
// isHoliday = 'yes';

let isMarried: null = null;

let is_married: undefined = undefined;

//reference value: object, array

//interface
interface Todo {
  id: number;
  name: string;
  isComplete: boolean;
}

interface NewTodo extends Todo {
  desc?: string; //optional property
}

let todo1: Todo = {
  id: 2,
  name: "Làm dự án cuối khóa",
  isComplete: false,
};

let todo2: Todo = {
  id: 3,
  name: "Lam Capstone",
  isComplete: false,
};

let todo3: NewTodo = {
  id: 4,
  name: "Lam bai tap",
  isComplete: false,
  // desc: 'difficult',
};

type User = {
  id: number;
  name: string;
  age: number;
};

let user1: User = {
  id: 11,
  name: "alicee",
  age: 2,
};

//array
// let colors: string[] = ['black', 'blue', 'green'];
let colors: Array<string> = ["black", "blue", "green"];
// Array<string> -> generic

colors.push("white");
// colors.push(2); //error

let todos: Todo[] = [{ id: 2, name: "homework", isComplete: true }];

//union type: cho phép 1 biến có thể chứa được nhiều loại dữ liệu cho trước

type ResponseTodoBE = null | Todo;

type ResponseAgeBE = null | number;

let userAge1: ResponseAgeBE = null;
userAge1 = 2;
// userAge1 = '2';

let todoDetail: ResponseTodoBE = null;
todoDetail = {
  id: 2,
  name: "lau nha",
  isComplete: false,
};

enum GenderType {
  male = 0,
  female = 1,
}

interface SV {
  id: string;
  name: string;
  genger: GenderType;
}

let sv1: SV = {
  id: "11",
  name: "Bob",
  genger: GenderType.female,
};

let value: any = "alice";
value = true;
value = 113;

interface Car {
  id: number;
  name: string;
  price: number;
  desc: string;
}

// Partial => chuyển từ require thành optional
let introduceCar = (carInfo: Partial<Car>) => {
  console.log("Thông tin xe");
  console.log(carInfo.name, carInfo.price);
};

introduceCar({
  name: "VinFet",
  price: 2,
});

// Readonly => để không sửa được dữ liệu
let car1: Readonly<Car> = {
  id: 22,
  name: "Range Rover",
  price: 1,
  desc: "beautifull",
};

// car1.name = 'aaa';

interface TodoProps {
  id: number;
  name: string;
  desc?: string;
}

let myTodo: Required<TodoProps> = {
  id: 2,
  name: "lam viec nha",
  desc: "15p nua lam sau",
};

//overloading && overriding trong js,ts