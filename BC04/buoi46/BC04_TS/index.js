console.log("ookkk");
var username = "Alice";
username = "true";
//ngầm định kiểu dữ liệu
// typescript = type + javascript
// type: loại dữ liệu
// variable:type = value
//Primitive value
var account = "admin123";
var userAge = 2;
userAge = 3;
// userAge = "3";
var isHoliday = true;
isHoliday = false;
// isHoliday = 'yes';
var isMarried = null;
var is_married = undefined;
var todo1 = {
    id: 2,
    name: "Làm dự án cuối khóa",
    isComplete: false
};
var todo2 = {
    id: 3,
    name: "Lam Capstone",
    isComplete: false
};
var todo3 = {
    id: 4,
    name: "Lam bai tap",
    isComplete: false
};
var user1 = {
    id: 11,
    name: "alicee",
    age: 2
};
//array
// let colors: string[] = ['black', 'blue', 'green'];
var colors = ["black", "blue", "green"];
// Array<string> -> generic
colors.push("white");
// colors.push(2); //error
var todos = [{ id: 2, name: "homework", isComplete: true }];
var userAge1 = null;
userAge1 = 2;
// userAge1 = '2';
var todoDetail = null;
todoDetail = {
    id: 2,
    name: "lau nha",
    isComplete: false
};
var GenderType;
(function (GenderType) {
    GenderType[GenderType["male"] = 0] = "male";
    GenderType[GenderType["female"] = 1] = "female";
})(GenderType || (GenderType = {}));
var sv1 = {
    id: "11",
    name: "Bob",
    genger: GenderType.female
};
var value = "alice";
value = true;
value = 113;
// Partial => chuyển từ require thành optional
var introduceCar = function (carInfo) {
    console.log("Thông tin xe");
    console.log(carInfo.name, carInfo.price);
};
introduceCar({
    name: "VinFet",
    price: 2
});
// Readonly => để không sửa được dữ liệu
var car1 = {
    id: 22,
    name: "Range Rover",
    price: 1,
    desc: "beautifull"
};
var myTodo = {
    id: 2,
    name: "lam viec nha",
    desc: "15p nua lam sau"
};
