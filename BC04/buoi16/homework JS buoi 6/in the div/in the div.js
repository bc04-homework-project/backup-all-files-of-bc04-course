function printDiv() {
  var result = "";

  for (var n = 1; n <= 10; n++) {
    if (n % 2 == 0) {
      result += `<div class="p-2" style="background: red; color: white">Div chẵn ${n} </div>
                  `;
    } else {
      result += `<div class="p-2" style="background: blue; color: white">Div lẻ ${n} </div>
                  `;
    }
    // console.log({n, result});
  }

  document.getElementById("result").innerHTML = result;
}
