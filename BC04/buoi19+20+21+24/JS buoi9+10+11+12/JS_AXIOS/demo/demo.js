//đồng bộ chạy trước - Synchronus
var result = null;

//bất đồng bộ đưa qua cuối event loop, vào queue đợi Synchronus chạy xong thì Asynchronus mới chạy sau; nên dòng console.log 'result' chạy trước Axios
axios({
  url: "https://62db6cb7e56f6d82a772862e.mockapi.io/sv",
  method: "GET",
})
  .then(function (res) {
    console.log("res: ", res);
    result = res;
  })
  .catch(function (err) {
    console.log("err: ", err);
  });
console.log('result: ', result);

