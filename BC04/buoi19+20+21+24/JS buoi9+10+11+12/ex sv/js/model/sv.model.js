function SinhVien(maSv, tenSv, loaiSv, diemToan, diemVan) {
    this.id = maSv;
    this.name = tenSv;
    this.type = loaiSv;
    this.math = diemToan;
    this.literature = diemVan;
    this.getScore = function () {
        return (this.math + this.literature) / 2;
    };
    this.getRank = function () {
        var dtb = this.getScore();
        if (dtb>5) {
            return 'goood'
        } else {
            return 'badd'
        }
    };    
}

function cat(name, age) {
    this.name = name;
    this.age = age;
}

var milo = new cat('milo', 3);
milo.name;
milo.age;

var black = new cat('white', 5);
black.name;
