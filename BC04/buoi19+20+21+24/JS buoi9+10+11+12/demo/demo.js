// object ~ array : pass by reference
// trong dữ liệu chứa dữ liệu là bình thường, nên trong value có thể chứa value, trong object cũng có thể chứa object



var pull = {
  name: "pull",
  age: 5,
  gender: "male",
  address: "Cao Thắng",
  children: ["đen", "vàng", "đỏ"],
  wife: {
    name: "bom",
    age: 1,
    gender: "female",
  },
  isMarried: true,
  bark: function (friend) {
    console.log("meo Meo0000 " + friend);
  },
  run1: ruun("mannn"),
  run2: ruun,
  run3: ruun2,
};

console.log('type offf 1: ', typeof hobao);

//tess1
function ruun(fenn) {
  console.log("run tu tu thoy", fenn);
}
pull.run2('fen oyy');
function ruun2(fenn2) {
  console.log("run tu tu thoy", fenn2);
  return 13;
}
pull.run3('fen 3 oy');
var num6 = pull.run3();
console.log('num6: ', num6);


console.log("pull.name  1: ", pull.name);
pull.name = "milo";
console.log("pull.name 2: ", pull.name);

//dynamic key
var key = 'name';
pull[key]; 

pull.bark("Jerry");

console.log("pull.wife", pull.wife);

var miu = pull;

console.log("pull.name 3: ", pull.name);
miu.name = "Miuuu";
console.log("miu.name 1: ", miu.name);
console.log("pull.name 4: ", pull.name);
console.log("pull.age 1: ", pull.age);
miu.age = 8;
console.log("pull.age 2: ", pull.age);


// tesss 3
var hobao = miu;
console.log('hobao 1: ', hobao);


// tesss 2
pull = 3;
console.log("miu 1: ", miu);
var miu = 4;
console.log("miu 2: ", miu);
console.log("miu.name 2: ", miu.name);

console.log("pull 1: ", pull);
 pull = 7;
console.log("pull 2: ", pull);
console.log("pull.name 5: ", pull.name);

console.log('hobao 2: ', hobao);
 hobao = miu;
console.log('hobao 3: ', hobao);
hobao = 15;
console.log('hobao 4: ', hobao);
console.log("miu 3: ", miu);


// tess 4
var sochann = [2,4,6,8];
console.log('sochann 1: ', sochann);
sochann = 3;
console.log('sochann 2: ', sochann);

const solee = [1,3,5,7];
console.log('solee 1: ', solee);
// solee = 2; //gây ra lỗi khi đổi value của const
// console.log('solee 2: ', solee);
// const num1 = 5; 
// num1 = 2; //lỗi tương tự như phía trên
solee[1] = 22;
console.log('solee 3: ', solee);
console.log('type offf 2: ', typeof solee);
console.log('type offf 3: ', typeof solee[2]);


//tess 5
function valuu1(num2) {
  // return var num2 = 4; - chỉ return giá trị cuối, không return var hay gì tương tự, sẽ báo lỗi
  console.log('day la tess 5', num2);
  return num5 = 3;
};

var num3 = valuu1;
console.log('num3: ', num3);
var num4 = num3(5);
console.log('num4: ', num4);


//tess 6
// Tất cả các kiểu primitive trong js đều là immutable.
// Khi so sánh 2 biến có kiểu primitive với nhau thì chúng sẽ so sánh giá trị:
console.log('Tess 6 - 1')
var a = 3;
function test1(b) {
    console.log(a === b);  // true
    // console.log(c === b)  // error - c is not defined
    var b = 3;
    console.log(a === b)  // true
    b = 4;
    console.log(a === b)  // false
};
test1(a);

console.log('Tess 6 - 2')
// Nhưng khi so sánh 2 kiểu object (không phải các kiểu primitive) thì chúng sẽ so sánh địa chỉ của 2 object đó với nhau:
var c = {item: [1, 2, 3, 1]}
function test2(d) {
    console.log(c === d)   // true - 2 identifier trỏ đến cùng 1 địa chỉ sơ cấp
    var d = {item: [1, 2, 3, 1]}; // khai báo lại nên d trỏ đến địa chỉ sơ cấp khác
    console.log(c === d)   // false
    console.log(c.item === d.item)   // false - giá trị của item là array - vẫn là kiểu object nên vẫn trỏ tới địa chỉ sơ cấp khác nhau, giống giá trị cuối nhưng khác địa chỉ thì khi so địa chỉ vẫn ra kết quả là khác nhau
    console.log(c.item[0] === d.item[0])   // true - giá trị number trong vd này ở array của item là dữ liệu ở địa chỉ cuối, có thểm xem là giá trị cuối cùng, nên khi so sánh thì chỉ so sánh giá trị với nhau
    console.log(c.item[0] === d.item[1])  // false
    console.log(c.item[0] === d.item[3])  // true
}
test2(c);
// có thể hiểu JS khi so sánh dữ liệu ở địa chỉ cuối cấp (với các giá trị có kiểu cơ bản như number, string, boolean,...) thì là so sánh trực tiếp giá trị với nhau, còn khi so sánh dữ liệu ở địa chỉ sơ cấp (như type object có nhiều hơn 1 địa chỉ) thì khi so sánh phải xem coi là đang so dữ liệu ở địa chỉ sơ cấp hay đang so ở địa chỉ thứ/cuối cấp.


//tess 7
console.log('Tess 7')
function changeStuff(a, b, c, d) {
  a = a * 10;
  b = {item: "check2"};
  c.item = "check2";
  console.log(num8.item === d.item);  // true
  d.item = "check 3"; // gán lại giá trị ở địa chỉ thứ cấp (cấp 2), không phải địa chỉ sơ cấp (cấp 1) nên dù là array vẫn xem như là mutable, khiến biến num8 bên ngoài hàm thay đổi. Hay có thể hiểu đơn giản hơn là gán giá trị mới trực tiếp vào biến d = value thì giá trị của biến num8 không bị thay đổi, còn gán thông qua một tham chiếu d.item = value thì giá trị của biến num 8 sẽ bị thay đổi.
  console.log(num8.item === d.item);  // true
}

var num7 = 17;
var obj1 = {item: "check1"};
var obj2 = {item: "check1"};
var num8 = {item: [1, 3, 5]};

changeStuff(num7, obj1, obj2, num8);
console.log('num7: ', num7);  // 17
console.log('obj1: ', obj1); // {item: "check1"}
console.log('obj2: ', obj2);// {item: "check2"}
console.log('num8: ', num8);
