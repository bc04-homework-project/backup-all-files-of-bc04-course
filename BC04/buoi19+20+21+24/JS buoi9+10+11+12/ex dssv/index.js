const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

// chức năng thêm sinh viên
var dssv = [];
//lấy thông tin từ localStorage
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson != null) {
  console.log("okkk");
  dssv = JSON.parse(dssvJson);
  //array khi convert thành json sẽ mất function, để có lại function ta sẽ map lại
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];

    dssv[index] = new SinhVien(
      sv.ten,
      sv.ma,
      sv.matKhau,
      sv.email,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }

  renderDSSV(dssv);
}

function themSV() {
  var newSv = layThongTinTuForm();
  console.log("newSv: ", newSv);

    var isValid = validator.kiemTraRong(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên không được rỗng"
    ) && validator.kiemTraDoDai(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên phải có 4 ký tự", 4, 4
    );

    isValid = isValid & 
    validator.kiemTraEmail(newSv.email, "spanEmailSV", "Email không được rỗng") ;



   isValid = isValid & 
    validator.kiemTraRong(
      newSv.ten,
      "spanTenSV",
      "Tên sinh viên không được rỗng"
    ) &
    validator.kiemTraRong(
      newSv.matKhau,
      "spanMatKhau",
      "Mật khẩu không được rỗng"
    ) &
    validator.kiemTraRong(newSv.toan, "spanToan", "Điểm toán không được rỗng") &
    validator.kiemTraRong(newSv.ly, "spanLy", "Điểm lý không được rỗng") &
    validator.kiemTraRong(newSv.hoa, "spanHoa", "Điểm hóa không được rỗng");


  if (isValid) {
    dssv.push(newSv);

    console.log("dssv: ", dssv);

    //tạo Json
    var dssvJson = JSON.stringify(dssv);
    //lưu Json vào localStorage
    localStorage.setItem("DSSV_LOCALSTORAGE", dssvJson);
  }

  renderDSSV(dssv);
}
