function layThongTinTuForm() {
  const maSv = document.getElementById("txtMaSV").value;
  const tenSv = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const matKhau = document.getElementById("txtPass").value;
  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;

  return new SinhVien(tenSv, maSv, matKhau, email, diemToan, diemLy, diemHoa);
}

function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}

//render array ra giao diện
function renderDSSV(svArr) {
  console.log("svArr: ", svArr);
  var contentHTML = "";
  for (var i = 0; i < svArr.length; i++) {
    var sv = svArr[i];
    var trContent = ` <tr> 
          <td>${sv.ma}</td>
          <td>${sv.ten}</td>
          <td>${sv.email}</td>
          <td>${sv.tinhDTB()}</td>
          <td>
          <button onclick="xoaSinhVien('${sv.ma}')" class="btn btn-danger">Xóa</button>
<button onclick="suaSinhVien('${sv.ma}')" class="btn btn-warning">Sửa</button>
</td>
          </tr>
          `;
    contentHTML += trContent;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function timKiemViTri(id, dssv) {
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    if (sv.ma == id) {
      return index;
    }
  }
  //không tìm thấy thì trả về -1
  return -1;
}

function xoaSinhVien(id) {
  console.log("id: ", id);

  // var index = dssv.findIndex(function (sv) {
  //   return sv.ma == id;
  // });

  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index != -1) {
    dssv.splice(index, 1);
    renderDSSV(dssv);
  }
}

function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index != -1) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
  }
}
