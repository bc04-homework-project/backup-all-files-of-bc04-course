const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";

var dsnv = [];

var dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);
if (dsnvJson != null) {
  dsnv = JSON.parse(dsnvJson);

  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];

    dsnv[index] = new NhanVien(
      nv.taiKhoan,
      nv.ten,
      nv.email,
      nv.matKhau,
      nv.ngayLam,
      nv.luongCoBan,
      nv.chucVu,
      nv.gioLamTrongThang,

    );
  }

  renderDSNV(dsnv);
}

document.getElementById('btnThemNV').addEventListener('click', function themNV() {
  var newNV = layThongTinTuForm();
  // console.log("newNV: ", newNV);

  var isValid1 = isValid(newNV);
  if (isValid1) {
    dsnv.push(newNV);

    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
  }

  renderDSNV(dsnv);
});

function xoaNV(id) {
  var index = timKiemViTri(id, dsnv);
  // console.log("index: ", index);
  if (index != -1) {
    dsnv.splice(index, 1);

    localStorage.setItem(DSNV_LOCALSTORAGE, JSON.stringify(dsnv));

    renderDSNV(dsnv);
  }
}

function suaNV(id) {
  var index = timKiemViTri(id, dsnv);
  // console.log("index: ", index);
  if (index != -1) {
    var sv = dsnv[index];
    showThongTinLenForm(sv);
  }
  document.getElementById("txtMaSV").disabled = true;
}

var capNhatNV = () => {
  var fixSV = layThongTinTuForm();
  var isValid1 = isValid(fixSV);
  var index = timKiemViTri(fixSV.ma, dsnv);
  if (isValid1) {
    dsnv[index] = fixSV;

    localStorage.setItem(DSNV_LOCALSTORAGE, JSON.stringify(dsnv));
  }

  renderDSNV(dsnv);
  resetForm();
  document.getElementById("txtMaSV").disabled = false;
};
