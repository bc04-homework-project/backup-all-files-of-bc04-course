function layThongTinTuForm() {
  let taiKhoan = document.getElementById("tknv").value;
  let ten = document.getElementById("name").value;
  let email = document.getElementById("email").value;
  let matKhau = document.getElementById("password").value;
  let ngayLam = document.getElementById("datepicker").value;
  let luongCoBan = document.getElementById("luongCB").value;
  let chucVu = document.getElementById("chucvu").value;
  let gioLamTrongThang = document.getElementById("gioLam").value;

  return new NhanVien(taiKhoan, ten, email, matKhau, ngayLam, luongCoBan, chucVu, gioLamTrongThang);
}

function renderDSNV(dsnv) {
  // console.log("dsnv: ", dsnv);
  var contentHTML = "";

  dsnv.forEach(nv => {
    var trContent = ` <tr> 
          <td>${nv.taiKhoan}</td>
          <td>${nv.ten}</td>
          <td>${nv.email}</td>
          <td>${nv.ngayLam}</td>
          <td>${nv.chucVu}</td>
          <td>${nv.tongLuong()}</td>
          <td>${nv.loaiNhanVien()}</td>

          <td>
          <button onclick="xoaNV('${
            nv.taiKhoan
          }')" class="btn btn-danger">Xóa</button>
<button onclick="suaNV('${nv.taiKhoan}')" class="btn btn-warning">Sửa</button>
</td>
          </tr>
          `;
    contentHTML += trContent;
  });
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function showThongTinLenForm(nv) {
  document.getElementById("txtManV").value = nv.ma;
  document.getElementById("txtTennV").value = nv.ten;
  document.getElementById("txtEmail").value = nv.email;
  document.getElementById("txtPass").value = nv.matKhau;
  document.getElementById("txtDiemToan").value = nv.toan;
  document.getElementById("txtDiemLy").value = nv.ly;
  document.getElementById("txtDiemHoa").value = nv.hoa;
}

function timKiemViTri(id, dsnv) {
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    if (nv.ma == id) {
      return index;
    }
  }
  return -1;
}

function resetForm() {
  document.getElementById("txtMaNV").value = "";
  document.getElementById("txtTenNV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
}
