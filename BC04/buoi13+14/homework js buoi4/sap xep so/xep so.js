function arrange() {
  var num1 = document.getElementById("num1").value * 1;
  var num2 = document.getElementById("num2").value * 1;
  var num3 = document.getElementById("num3").value * 1;

  var result = document.getElementById("result");

  if (num1 > num2 && num2 > num3) {
    result.innerHTML = `${num3} < ${num2} < ${num1}`;
  } else if (num1 > num3 && num3 > num2) {
    result.innerHTML = `${num2} < ${num3} < ${num1}`;
  } else if (num2 > num3 && num3 > num1) {
    result.innerHTML = `${num1} < ${num3} < ${num2}`;
  } else if (num2 > num1 && num1 > num3) {
    result.innerHTML = `${num3} < ${num1} < ${num2}`;
  } else if (num3 > num2 && num2 > num1) {
    result.innerHTML = `${num1} < ${num2} < ${num3}`;
  } else {
    result.innerHTML = `${num2} < ${num1} < ${num3}`;
  }

  // console.log('result: ', result);
}
