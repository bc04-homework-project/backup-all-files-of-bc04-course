// function count() {
//   var num1 = document.getElementById("num1").value * 1;
//   var num2 = document.getElementById("num2").value * 1;
//   var num3 = document.getElementById("num3").value * 1;

//   var countOdd = (num1 % 2) + (num2 % 2) + (num3 % 2);
//   // console.log('countOdd: ', countOdd);
//   var countEven = 3 - countOdd;
//   // console.log('countEven: ', countEven);

//   document.getElementById(
//     "result"
//   ).innerHTML = `Có <span class="text-primary"> ${countEven} </span> số chẵn và <span class="text-success"> ${countOdd} </span> số lẻ`;
// }


// Một cách làm khác

function count() {
  var num1 = document.getElementById("num1").value * 1;
  var num2 = document.getElementById("num2").value * 1;
  var num3 = document.getElementById("num3").value * 1;

  var countOdd = 0;
  
  num1 % 2 != 0 && countOdd++;
  num2 % 2 != 0 && countOdd++;
  num3 % 2 != 0 && countOdd++;
  console.log('countOdd: ', countOdd);

  var countEven = 3 - countOdd;
  console.log('countEven: ', countEven);

  document.getElementById(
    "result"
  ).innerHTML = `Có <span class="text-primary"> ${countEven} </span> số chẵn và <span class="text-success"> ${countOdd} </span> số lẻ`;
}
