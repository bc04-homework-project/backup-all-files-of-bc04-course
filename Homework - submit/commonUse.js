function layThongTinTuForm() {
    let taiKhoan = document.getElementById("tknv").value;
    let ten = document.getElementById("name").value;
    let email = document.getElementById("email").value;
    let matKhau = document.getElementById("password").value;
    let ngayLam = document.getElementById("datepicker").value;
    let luongCoBan = document.getElementById("luongCB").value;
    let chucVu = document.getElementById("chucvu").value;
    let gioLamTrongThang = document.getElementById("gioLam").value;
  
    return new NhanVien(
      taiKhoan,
      ten,
      email,
      matKhau,
      ngayLam,
      luongCoBan,
      chucVu,
      gioLamTrongThang
    );
  }
  
  function renderDSNV(dsnv) {
    // console.log("dsnv: ", dsnv);
    var contentHTML = "";
  
    dsnv.forEach((nv) => {
      var trContent = ` <tr> 
            <td>${nv.taiKhoan}</td>
            <td>${nv.ten}</td>
            <td>${nv.email}</td>
            <td>${nv.ngayLam}</td>
            <td>${nv.chucVu}</td>
            <td>${nv.tongLuong()}</td>
            <td>${nv.loaiNhanVien()}</td>
  
            <td>
            <button onclick="xoaNV('${
              nv.taiKhoan
            }')" class="btn btn-danger">Xóa</button>
  <button onclick="suaNV('${
        nv.taiKhoan
      }')" class="btn btn-warning" data-toggle="modal"
  data-target="#myModal">Sửa</button>
  </td>
            </tr>
            `;
      contentHTML += trContent;
    });
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
  }
  
  function showThongTinLenForm(nv) {
    document.getElementById("tknv").value = nv.taiKhoan;
    document.getElementById("name").value = nv.ten;
    document.getElementById("email").value = nv.email;
    document.getElementById("password").value = nv.matKhau;
    document.getElementById("datepicker").value = nv.ngayLam;
    document.getElementById("luongCB").value = nv.luongCoBan;
    document.getElementById("chucvu").value = nv.chucVu;
    document.getElementById("gioLam").value = nv.gioLamTrongThang;
  }
  
  function timKiemViTri(account, dsnv) {
    for (var index = 0; index < dsnv.length; index++) {
      var nv = dsnv[index];
      if (nv.taiKhoan == account) {
        return index;
      }
    }
    return -1;
  }
  export let timKiemViTri = (id, glassesList) => {
    let index = glassesList.findIndex((glasses) => {
      return glasses.id == id;
    });
  
    return index;
  };
  
  
  function searchNV() {
    let timNV = document.getElementById("searchName").value;
  
    let nvXuatSac = [];
    let nvGioi = [];
    let nvKha = [];
    let nvTB = [];
  
    dsnv.forEach((nv) => {
      if (nv.loaiNhanVien() == "Nhân viên xuất sắc") {
        nvXuatSac.push(nv);
      } else if (nv.loaiNhanVien() == "Nhân viên giỏi") {
        nvGioi.push(nv);
      } else if (nv.loaiNhanVien() == "Nhân viên khá") {
        nvKha.push(nv);
      } else {
        nvTB.push(nv);
      }
    });
  
    ["xuat sac", "xuất sắc"].includes(timNV.toLowerCase())
      ? renderDSNV(nvXuatSac)
      : ["gioi", "giỏi"].includes(timNV.toLowerCase())
      ? renderDSNV(nvGioi)
      : ["kha", "khá"].includes(timNV.toLowerCase())
      ? renderDSNV(nvKha)
      : ["trung binh", "trung bình"].includes(timNV.toLowerCase())
      ? renderDSNV(nvTB)
      : (document.getElementById(
          "tableDanhSach"
        ).innerHTML = `Không tìm thấy thông tin bạn nhập <br> Loại nhân viên chỉ bao gồm Xuất sắc / Giỏi / Khá / Trung Bình`);
  }
  
  document
    .getElementById("btnDong")
    .addEventListener("click", function dongForm() {
      document.getElementById("tknv").disabled = false;
      document.getElementById("btnThemNV").disabled = false;
    });
  

//data to test
function dataTotest(num) {
    for (let n = 1; n <= num; n++) {
      let nv = new NhanVien(
        "acc" + n,
        "Tran Van " + String.fromCharCode(65 + n),
        "mail@g.co" + n,
        "abcDEF@!" + n,
        `08/22/2021`,
        n < 20 ? n * 1e6 : 2e7,
        n < num - 2 ? "Nhân viên" : n < num ? "Trưởng phòng" : "Sếp",
        n < 12 ? 80 + n * 10 : 200
      );
      dsnv.push(nv);
    }
  }
  
  

    //các hàm sau thường dùng trong file index.js 
  

    const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";

var dsnv = [];
dataTotest(12); //set sẵn một ít data để đỡ phải nhập tay lần đầu
renderDSNV(dsnv);

var dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);
if (dsnvJson != null) {
  dsnv = JSON.parse(dsnvJson);

  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];

    dsnv[index] = new NhanVien(
      nv.taiKhoan,
      nv.ten,
      nv.email,
      nv.matKhau,
      nv.ngayLam,
      nv.luongCoBan,
      nv.chucVu,
      nv.gioLamTrongThang
    );
  }
  renderDSNV(dsnv);
}

document
  .getElementById("btnThemNV")
  .addEventListener("click", function themNV() {
    var newNV = layThongTinTuForm();
    // console.log("newNV: ", newNV);

    var isValid1 = isValid(newNV);
    if (isValid1) {
      dsnv.push(newNV);

      var dsnvJson = JSON.stringify(dsnv);
      localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
    }

    renderDSNV(dsnv);
  });

function xoaNV(account) {
  var index = timKiemViTri(account, dsnv);
  // console.log("index: ", index);
  if (index != -1) {
    dsnv.splice(index, 1);

    localStorage.setItem(DSNV_LOCALSTORAGE, JSON.stringify(dsnv));

    renderDSNV(dsnv);
  }
}

function suaNV(account) {
  var index = timKiemViTri(account, dsnv);
  // console.log("index: ", index);
  if (index != -1) {
    var nv = dsnv[index];
    showThongTinLenForm(nv);
  }
  document.getElementById("tknv").disabled = true;
  document.getElementById("btnThemNV").disabled = true;
}

document
  .getElementById("btnCapNhat")
  .addEventListener("click", function capNhatNV() {
    var fixNV = layThongTinTuForm();
    var isValid1 = isValid(fixNV);
    var index = timKiemViTri(fixNV.taiKhoan, dsnv);
    if (isValid1) {
      dsnv[index] = fixNV;

      localStorage.setItem(DSNV_LOCALSTORAGE, JSON.stringify(dsnv));
    }

    renderDSNV(dsnv);
    document.getElementById("tknv").disabled = false;
    document.getElementById("btnThemNV").disabled = false;
  });

//search nhân viên theo xếp loại
document.getElementById("btnTimNV").addEventListener("click", () => {
  searchNV();
});
document.getElementById("searchName").addEventListener("keydown", (e) => {
  if (e.key === "Enter") {
    searchNV();
  }
});

